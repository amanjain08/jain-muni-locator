package com.architjn.jainmunilocator;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.architjn.jainmunilocator.AppUtil.AppValues;
import com.architjn.jainmunilocator.AppUtil.LaunchReceiver;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

/**
 * Created by Aman Jain on 22/05/16.
 */
public class MonitorService extends Service {

    private static final String TAG = "MonitorService";
    private AlarmManager alarms;
    private PendingIntent alarmIntent;
    String city = "";

    @Override
    public void onCreate() {
        super.onCreate();
        AppValues.getInstance().getData(MonitorService.this);
        alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intentOnAlarm = new Intent(LaunchReceiver.ACTION_PULSE_SERVER_ALARM);
        alarmIntent = PendingIntent.getBroadcast(this, 0, intentOnAlarm, 0);

    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        // reload our data
        if (ActivityCompat.checkSelfPermission(MonitorService.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (SmartLocation.with(this).location().state().locationServicesEnabled() && SmartLocation.with(this).location().state().isNetworkAvailable() && AppValues.isInternetConnected(MonitorService.this)) {
                SmartLocation.with(this).location().oneFix().start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        if (location != null) {
                            String address = getAddress(location.getLatitude(), location.getLongitude());
                            try {
                                String URL = "http://jainmunilocator.org/map/updateLocation.php?email=" + AppValues.email.replaceAll(" ", "%20") + "&username=" + AppValues.name.replaceAll(" ", "%20") + "&userimg=" + AppValues.imgUrl.replaceAll(" ", "%20") + "&userlat=" + location.getLatitude() + "&userlng=" + location.getLongitude() + "&userlocation=" + address.replaceAll(" ", "%20") + "&userlocality=" + city.replaceAll(" ", "%20");
                                Log.d(TAG, "onLocationUpdated: " + URL);
                                StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                int interval = 5;
                                                long timeToAlarm = SystemClock.elapsedRealtime() + interval * 1000 * 36 * 36;
                                                alarms.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, timeToAlarm, interval, alarmIntent);
                                                SmartLocation.with(MonitorService.this).location().stop();
                                                stopSelf();
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                int interval = 5;
                                                long timeToAlarm = SystemClock.elapsedRealtime() + interval * 1000 * 36 * 36;
                                                alarms.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, timeToAlarm, interval, alarmIntent);
                                                stopSelf();
                                            }
                                        }
                                );
                                AppController.getInstance().addToRequestQueue(stringRequest);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private String getAddress(double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                int max = address.getMaxAddressLineIndex();
                for (int i = 0; i < max; i++) {
                    if (i == 0)
                        result.append(address.getAddressLine(i));
                    else if (i == max - 1)
                        result.append(", " + address.getAddressLine(i).substring(0, address.getAddressLine(i).length() - 7));
                    else
                        result.append(", " + address.getAddressLine(i));

                }
                Log.d(TAG, "getAddress: " + address.toString() + "\n");
                if (address.getLocality() != null) {
                    city = address.getLocality();

                }


            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return result.toString();
    }

}