package com.architjn.jainmunilocator.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.architjn.jainmunilocator.Activity.MainActivity;
import com.architjn.jainmunilocator.Activity.PickLocationActivity;
import com.architjn.jainmunilocator.AppController;
import com.architjn.jainmunilocator.AppUtil.AppValues;
import com.architjn.jainmunilocator.Models.CompleteMuniDetail;
import com.architjn.jainmunilocator.Models.ContactModel;
import com.architjn.jainmunilocator.Models.HeaderDetailModel;
import com.architjn.jainmunilocator.Models.HistoryDataModel;
import com.architjn.jainmunilocator.Models.PadDetailModel;
import com.architjn.jainmunilocator.R;
import com.google.android.gms.maps.model.LatLng;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by audiocompass on 29/05/16.
 */
public class MuniDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MUNIDETAIL";
    private static int TYPE_HEADER = 0;
    private static int TYPE_PAD = 1;
    private static int TYPE_HISTORY = 2;
    private static int TYPE_CONTACT = 3;

    protected Activity mActivity;
    protected CompleteMuniDetail muniData;
    private HistoryDataModel histroyData;
    private HeaderDetailModel headerData;
    private List<PadDetailModel> padDetailList = new ArrayList<PadDetailModel>();
    private ContactModel contactData;
    private int CUSTOM_AUTOCOMPLETE_REQUEST_CODE = 13;
    PadDetailViewHolder tempPadDetailViewHolder;
    HistoryViewHolder tempHistoryViewHolder;
    LatLng latLng;
    static final int DATE_PICKER_ID = 1111;
    boolean history, pad;
    FilterWithSpaceAdapter<String> adapter;


    public MuniDetailAdapter(Activity mActivity, CompleteMuniDetail muniData, HeaderDetailModel headerData, List<PadDetailModel> padDetailList, HistoryDataModel histroyData, ContactModel contactData) {
        this.mActivity = mActivity;
        this.muniData = muniData;
        this.histroyData = histroyData;
        this.headerData = headerData;
        this.padDetailList = padDetailList;
        this.contactData = contactData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (viewType == TYPE_HEADER) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.muni_detail_header, parent, false);
            return new HeaderViewHolder(v);
        } else if (viewType == TYPE_PAD) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pad_detail, parent, false);
            return new PadDetailViewHolder(v);

        } else if (viewType == TYPE_HISTORY) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history, parent, false);
            return new HistoryViewHolder(v);
        } else if (viewType == TYPE_CONTACT) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.muni_contact, parent, false);
            return new ContactViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            if (!headerData.getTitle().equalsIgnoreCase("Last Location Detail")) {
                headerViewHolder.samadhiText.setVisibility(View.VISIBLE);
            }
            headerViewHolder.name.setText(headerData.getName());
            headerViewHolder.lastLocation.setText(headerData.getLastLocation());
            headerViewHolder.time.setText(headerData.getTime());
        } else if (holder instanceof PadDetailViewHolder) {

            final PadDetailModel padData = padDetailList.get(position - 1);
            final int[] index = {-999};
            final String[] guruID = {String.valueOf(padData.getGuruId())};
            final PadDetailViewHolder padViewHolder = (PadDetailViewHolder) holder;
            padViewHolder.deekshaTitle.setText(padData.getTitle());
            if (padData.getName() != null) {
                padViewHolder.name.setText(AppValues.checkString(padData.getName()));
            } else {
                padViewHolder.name.setText(AppValues.checkString(""));
            }
            padViewHolder.deekshaDate.setText(AppValues.checkString(padData.getDate()));

            if (padData.getGuruId() != 0 && MainActivity.muniList.size() > padData.getGuruId()) {
                padViewHolder.deekshaGuru.setText(AppValues.checkString(AppValues.getName(MainActivity.muniList.get(padData.getGuruId() - 1))));
            } else
                padViewHolder.deekshaGuru.setText(AppValues.checkString(""));

            padViewHolder.place.setText(AppValues.checkString(padData.getPlace()));
            padViewHolder.edit.setTag("Edit");
            padViewHolder.placeEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        Intent i = new Intent(mActivity, PickLocationActivity.class);
                        i.putExtra("prevLoc", AppValues.isData(padData.getPlace()));
                        mActivity.startActivityForResult(i, CUSTOM_AUTOCOMPLETE_REQUEST_CODE);

                        tempPadDetailViewHolder = padViewHolder;
                        hideKeyboard(v);
                        padViewHolder.placeEdit.clearFocus();
                        pad = true;

                    }
                }
            });
            padViewHolder.dateEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        mActivity.showDialog(DATE_PICKER_ID);
                        tempPadDetailViewHolder = padViewHolder;
                        hideKeyboard(v);
                        padViewHolder.dateEdit.clearFocus();
                        pad = true;
                    }
                }
            });
            adapter = new FilterWithSpaceAdapter<String>(mActivity, R.layout.list_item, R.id.suggestion_text, MainActivity.muniName);
            padViewHolder.guruEdit.setAdapter(adapter);
            padViewHolder.guruEdit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    index[0] = MainActivity.muniNameID.get(parent.getItemAtPosition(position));
                    padViewHolder.guruEdit.setText(AppValues.getName(MainActivity.muniList.get(position)));
                    guruID[0] = MainActivity.muniList.get(position).getId().toString();
                    hideKeyboard(view);
                    padViewHolder.guruEdit.clearFocus();
                }
            });

            padViewHolder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (padViewHolder.edit.getTag() != null && padViewHolder.edit.getTag().toString().equalsIgnoreCase("Edit")) {
                        padViewHolder.edit.setImageResource(R.drawable.ic_clear_black_24dp);
                        padViewHolder.edit.setTag("Close");
                        padViewHolder.editLayout.setVisibility(View.VISIBLE);
                        padViewHolder.expandedLayout.setVisibility(View.GONE);
                        switchView(padViewHolder.deekshaDate, padViewHolder.dateEdit, "Edit", false);
                        switchView(padViewHolder.deekshaGuru, padViewHolder.guruEdit, "Edit", false);
                        switchView(padViewHolder.place, padViewHolder.placeEdit, "Edit", false);
                    } else {
                        padViewHolder.editLayout.setVisibility(View.GONE);
                        padViewHolder.expandedLayout.setVisibility(View.VISIBLE);
                        padViewHolder.edit.setImageResource(R.drawable.ic_edit_black_24dp);
                        padViewHolder.edit.setTag("Edit");
                        switchView(padViewHolder.deekshaDate, padViewHolder.dateEdit, "Close", false);
                        switchView(padViewHolder.deekshaGuru, padViewHolder.guruEdit, "Close", false);
                        switchView(padViewHolder.place, padViewHolder.placeEdit, "Close", false);
                    }
                }
            });
            padViewHolder.cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    padViewHolder.edit.setImageResource(R.drawable.ic_edit_black_24dp);
                    padViewHolder.edit.setTag("Edit");
                    padViewHolder.editLayout.setVisibility(View.GONE);
                    padViewHolder.expandedLayout.setVisibility(View.VISIBLE);
                    switchView(padViewHolder.deekshaDate, padViewHolder.dateEdit, "Close", false);
                    switchView(padViewHolder.deekshaGuru, padViewHolder.guruEdit, "Close", false);
                    switchView(padViewHolder.place, padViewHolder.placeEdit, "Close", false);
                }
            });
            padViewHolder.submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    padViewHolder.progressBar.setVisibility(View.VISIBLE);
                    String url = "http://jainmunilocator.org/munis/setpad.php";
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, "onResponse: " + response);
                            padViewHolder.progressBar.setVisibility(View.GONE);
                            padViewHolder.edit.setImageResource(R.drawable.ic_edit_black_24dp);
                            padViewHolder.edit.setTag("Edit");
                            padViewHolder.editLayout.setVisibility(View.GONE);
                            switchView(padViewHolder.deekshaDate, padViewHolder.dateEdit, "Close", true);
                            switchView(padViewHolder.deekshaGuru, padViewHolder.guruEdit, "Close", true);
                            switchView(padViewHolder.place, padViewHolder.placeEdit, "Close", true);
                            padViewHolder.expandedLayout.setVisibility(View.VISIBLE);

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            padViewHolder.progressBar.setVisibility(View.GONE);
                            Toast.makeText(mActivity, "Error Occured. Try Again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
                            String pad = padData.getPrefix();
                            params.put("pad", pad);
                            params.put(pad + "id", padData.getId());
                            params.put(pad + "editor", AppValues.email);
                            params.put(pad + "mode", "android");
                            String finalDate = "0000-00-00";
                            if (!padViewHolder.dateEdit.getText().toString().isEmpty()) {
                                SimpleDateFormat myTime = new SimpleDateFormat("EEEE, dd MMMM yyyy");
                                SimpleDateFormat wantsTime = new SimpleDateFormat("yyyy-MM-dd");
                                try {
                                    finalDate = wantsTime.format(myTime.parse(padViewHolder.dateEdit.getText().toString()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                            params.put(pad + "date", finalDate);
                            params.put(pad + "guru", guruID[0]);
                            params.put(pad + "place", padViewHolder.placeEdit.getText().toString());
                            if (latLng != null) {
                                params.put(pad + "lat", String.valueOf(latLng.latitude));
                                params.put(pad + "lng", String.valueOf(latLng.longitude));
                            } else if (!String.valueOf(padData.getLat()).equalsIgnoreCase("0.000000")) {
                                params.put(pad + "lat", String.valueOf(padData.getLat()));
                                params.put(pad + "lng", String.valueOf(padData.getLon()));
                            } else {
                                params.put(pad + "lat", "0.000000");
                                params.put(pad + "lng", "0.000000");
                            }

                            Log.d(TAG, "from params " + pad + ": " + params.toString());
                            return AppValues.checkParams(params);
                        }
                    };
                    AppController.getInstance().addToRequestQueue(stringRequest, "pad");

                }
            });

        } else if (holder instanceof HistoryViewHolder) {
            final HistoryViewHolder historyViewHolder = (HistoryViewHolder) holder;
            setHistory(histroyData.getBirthName(), historyViewHolder.birthName, historyViewHolder.birthNameText);
            setHistory(histroyData.getDob(), historyViewHolder.dob, historyViewHolder.dobText);
            setHistory(histroyData.getBirthPlace(), historyViewHolder.birthPlace, historyViewHolder.birthPlaceText);
            setHistory(histroyData.getFatherName(), historyViewHolder.father, historyViewHolder.fatherText);
            setHistory(histroyData.getMotherName(), historyViewHolder.mother, historyViewHolder.motherText);
            setHistory(histroyData.getEducation(), historyViewHolder.education, historyViewHolder.educationText);
            historyViewHolder.edit.setTag("Edit");

            historyViewHolder.birthPlaceEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        Intent i = new Intent(mActivity, PickLocationActivity.class);
                        i.putExtra("prevLoc", AppValues.isData(histroyData.getBirthPlace()));
                        mActivity.startActivityForResult(i, CUSTOM_AUTOCOMPLETE_REQUEST_CODE);
                        //  mActivity.overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top);
                        tempHistoryViewHolder = historyViewHolder;
                        historyViewHolder.birthPlaceEdit.clearFocus();
                        hideKeyboard(v);
                        history = true;
                    }
                }
            });

            historyViewHolder.dobEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    mActivity.showDialog(DATE_PICKER_ID);
                    tempHistoryViewHolder = historyViewHolder;
                    historyViewHolder.dobEdit.clearFocus();
                    hideKeyboard(v);
                    history = true;
                }
            });
            historyViewHolder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (historyViewHolder.edit.getTag() != null && historyViewHolder.edit.getTag().toString().equalsIgnoreCase("Edit")) {
                        historyViewHolder.edit.setImageResource(R.drawable.ic_clear_black_24dp);
                        historyViewHolder.edit.setTag("Close");
                        historyViewHolder.editLayout.setVisibility(View.VISIBLE);
                        historyViewHolder.expandedLayout.setVisibility(View.GONE);
                        switchView(historyViewHolder.birthName, historyViewHolder.birthNameEdit, "Edit", false);
                        switchView(historyViewHolder.dob, historyViewHolder.dobEdit, "Edit", false);
                        switchView(historyViewHolder.birthPlace, historyViewHolder.birthPlaceEdit, "Edit", false);
                        switchView(historyViewHolder.father, historyViewHolder.fatherEdit, "Edit", false);
                        switchView(historyViewHolder.mother, historyViewHolder.motherEdit, "Edit", false);
                        switchView(historyViewHolder.education, historyViewHolder.educationEdit, "Edit", false);
                    } else {
                        historyViewHolder.edit.setImageResource(R.drawable.ic_edit_black_24dp);
                        historyViewHolder.edit.setTag("Edit");
                        historyViewHolder.editLayout.setVisibility(View.GONE);
                        historyViewHolder.expandedLayout.setVisibility(View.VISIBLE);
                        switchView(historyViewHolder.birthName, historyViewHolder.birthNameEdit, "Close", false);
                        switchView(historyViewHolder.dob, historyViewHolder.dobEdit, "Close", false);
                        switchView(historyViewHolder.birthPlace, historyViewHolder.birthPlaceEdit, "Close", false);
                        switchView(historyViewHolder.father, historyViewHolder.fatherEdit, "Close", false);
                        switchView(historyViewHolder.mother, historyViewHolder.motherEdit, "Close", false);
                        switchView(historyViewHolder.education, historyViewHolder.educationEdit, "Close", false);
                    }
                }
            });
            historyViewHolder.cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    historyViewHolder.edit.setImageResource(R.drawable.ic_edit_black_24dp);
                    historyViewHolder.edit.setTag("Edit");
                    historyViewHolder.editLayout.setVisibility(View.GONE);
                    historyViewHolder.expandedLayout.setVisibility(View.VISIBLE);
                    switchView(historyViewHolder.birthName, historyViewHolder.birthNameEdit, "Close", false);
                    switchView(historyViewHolder.dob, historyViewHolder.dobEdit, "Close", false);
                    switchView(historyViewHolder.birthPlace, historyViewHolder.birthPlaceEdit, "Close", false);
                    switchView(historyViewHolder.father, historyViewHolder.fatherEdit, "Close", false);
                    switchView(historyViewHolder.mother, historyViewHolder.motherEdit, "Close", false);
                    switchView(historyViewHolder.education, historyViewHolder.educationEdit, "Close", false);


                }
            });
            historyViewHolder.submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    historyViewHolder.progressBar.setVisibility(View.VISIBLE);
                    String url = "http://jainmunilocator.org/munis/sethistory.php";
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, "onResponse: " + response);
                            historyViewHolder.edit.setImageResource(R.drawable.ic_edit_black_24dp);
                            historyViewHolder.edit.setTag("Edit");
                            historyViewHolder.editLayout.setVisibility(View.GONE);
                            switchView(historyViewHolder.birthName, historyViewHolder.birthNameEdit, "Close", true);
                            switchView(historyViewHolder.dob, historyViewHolder.dobEdit, "Close", true);
                            switchView(historyViewHolder.birthPlace, historyViewHolder.birthPlaceEdit, "Close", true);
                            switchView(historyViewHolder.father, historyViewHolder.fatherEdit, "Close", true);
                            switchView(historyViewHolder.mother, historyViewHolder.motherEdit, "Close", true);
                            switchView(historyViewHolder.education, historyViewHolder.educationEdit, "Close", true);
                            historyViewHolder.expandedLayout.setVisibility(View.VISIBLE);

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            historyViewHolder.progressBar.setVisibility(View.GONE);
                            Toast.makeText(mActivity, "Error Occured. Try Again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
                            params.put("historyid", muniData.getId());
                            params.put("historyeditor", AppValues.email);
                            params.put("historymode","android");
                            params.put(AppValues.loginAgent,AppValues.userID);
                            params.put("birthname", historyViewHolder.birthNameEdit.getText().toString());
                            String finalDate = "0000-00-00";
                            if (!historyViewHolder.dobEdit.getText().toString().isEmpty()) {
                                SimpleDateFormat myTime = new SimpleDateFormat("EEEE, dd MMMM yyyy");
                                SimpleDateFormat wantsTime = new SimpleDateFormat("yyyy-MM-dd");
                                try {
                                    finalDate = wantsTime.format(myTime.parse(historyViewHolder.dobEdit.getText().toString()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                            params.put("dob", finalDate);
                            params.put("birthplace", historyViewHolder.birthPlaceEdit.getText().toString());
                            if (latLng != null) {
                                params.put("birthlat", String.valueOf(latLng.latitude));
                                params.put("birthlng", String.valueOf(latLng.longitude));
                            } else if (histroyData.getBirhtLat().equalsIgnoreCase("0.000000")) {
                                params.put("birthlat", histroyData.getBirhtLat());
                                params.put("birthlng", histroyData.getBirthLng());
                            } else {
                                params.put("birthlat", "0.000000");
                                params.put("birthlng", "0.000000");
                            }
                            params.put("father", historyViewHolder.fatherEdit.getText().toString());
                            params.put("mother", historyViewHolder.motherEdit.getText().toString());
                            params.put("education", historyViewHolder.educationEdit.getText().toString());
                            Log.d(TAG, "from params contact : " + params.toString());
                            return AppValues.checkParams(params);
                        }
                    };
                    AppController.getInstance().addToRequestQueue(stringRequest, "history");
                }
            });


        } else if (holder instanceof ContactViewHolder) {
            final ContactViewHolder contactViewHolder = (ContactViewHolder) holder;
            contactViewHolder.phone.setText(AppValues.checkString(contactData.getPhone()));
            setContact(contactData.getEmail(), contactViewHolder.email, contactViewHolder.email_icon);
            setContact(contactData.getWebsite(), contactViewHolder.web, contactViewHolder.web_icon);
            setContact(contactData.getFacebook(), contactViewHolder.facebook, contactViewHolder.facebook_icon);
            setContact(contactData.getGplus(), contactViewHolder.gplus, contactViewHolder.gplus_icon);
            setContact(contactData.getYoutube(), contactViewHolder.youtube, contactViewHolder.youtube_icon);
            setContact(contactData.getWikipedia(), contactViewHolder.wikipedia, contactViewHolder.wikipedia_icon);

            contactViewHolder.edit.setTag("Edit");
            contactViewHolder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (contactViewHolder.edit.getTag() != null && contactViewHolder.edit.getTag().toString().equalsIgnoreCase("Edit")) {
                        contactViewHolder.edit.setImageResource(R.drawable.ic_clear_black_24dp);
                        contactViewHolder.edit.setTag("Close");
                        contactViewHolder.editLayout.setVisibility(View.VISIBLE);
                        contactViewHolder.expandedLayout.setVisibility(View.GONE);
                        switchView(contactViewHolder.phone, contactViewHolder.phone_edit, "Edit", false);
                        switchView(contactViewHolder.email, contactViewHolder.email_edit, "Edit", false);
                        switchView(contactViewHolder.web, contactViewHolder.web_edit, "Edit", false);
                        switchView(contactViewHolder.facebook, contactViewHolder.facebook_edit, "Edit", false);
                        switchView(contactViewHolder.gplus, contactViewHolder.gplus_edit, "Edit", false);
                        switchView(contactViewHolder.youtube, contactViewHolder.youtube_edit, "Edit", false);
                        switchView(contactViewHolder.wikipedia, contactViewHolder.wikipedia_edit, "Edit", false);
                    } else {
                        contactViewHolder.edit.setImageResource(R.drawable.ic_edit_black_24dp);
                        contactViewHolder.edit.setTag("Edit");
                        contactViewHolder.editLayout.setVisibility(View.GONE);
                        contactViewHolder.expandedLayout.setVisibility(View.VISIBLE);
                        switchView(contactViewHolder.phone, contactViewHolder.phone_edit, "Close", false);
                        switchView(contactViewHolder.email, contactViewHolder.email_edit, "Close", false);
                        switchView(contactViewHolder.web, contactViewHolder.web_edit, "Close", false);
                        switchView(contactViewHolder.facebook, contactViewHolder.facebook_edit, "Close", false);
                        switchView(contactViewHolder.gplus, contactViewHolder.gplus_edit, "Close", false);
                        switchView(contactViewHolder.youtube, contactViewHolder.youtube_edit, "Close", false);
                        switchView(contactViewHolder.wikipedia, contactViewHolder.wikipedia_edit, "Close", false);
                    }
                }
            });
            contactViewHolder.cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    contactViewHolder.edit.setImageResource(R.drawable.ic_edit_black_24dp);
                    contactViewHolder.edit.setTag("Edit");
                    contactViewHolder.editLayout.setVisibility(View.GONE);
                    contactViewHolder.expandedLayout.setVisibility(View.VISIBLE);
                    switchView(contactViewHolder.phone, contactViewHolder.phone_edit, "Close", false);
                    switchView(contactViewHolder.email, contactViewHolder.email_edit, "Close", false);
                    switchView(contactViewHolder.web, contactViewHolder.web_edit, "Close", false);
                    switchView(contactViewHolder.facebook, contactViewHolder.facebook_edit, "Close", false);
                    switchView(contactViewHolder.gplus, contactViewHolder.gplus_edit, "Close", false);
                    switchView(contactViewHolder.youtube, contactViewHolder.youtube_edit, "Close", false);
                    switchView(contactViewHolder.wikipedia, contactViewHolder.wikipedia_edit, "Close", false);
                }
            });
            contactViewHolder.submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    contactViewHolder.progressBar.setVisibility(View.VISIBLE);
                    String url = "http://jainmunilocator.org/munis/setcontact.php";
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, "onResponse: " + response);
                            contactViewHolder.progressBar.setVisibility(View.GONE);
                            contactViewHolder.edit.setImageResource(R.drawable.ic_edit_black_24dp);
                            contactViewHolder.edit.setTag("Edit");
                            contactViewHolder.editLayout.setVisibility(View.GONE);
                            switchView(contactViewHolder.phone, contactViewHolder.phone_edit, "Close", true);
                            switchView(contactViewHolder.email, contactViewHolder.email_edit, "Close", true);
                            switchView(contactViewHolder.web, contactViewHolder.web_edit, "Close", true);
                            switchView(contactViewHolder.facebook, contactViewHolder.facebook_edit, "Close", true);
                            switchView(contactViewHolder.gplus, contactViewHolder.gplus_edit, "Close", true);
                            switchView(contactViewHolder.youtube, contactViewHolder.youtube_edit, "Close", true);
                            switchView(contactViewHolder.wikipedia, contactViewHolder.wikipedia_edit, "Close", true);
                            contactViewHolder.expandedLayout.setVisibility(View.VISIBLE);

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            contactViewHolder.progressBar.setVisibility(View.GONE);
                            Toast.makeText(mActivity, "Error Occured. Try Again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
                            params.put("contactid", muniData.getId());
                            params.put("contacteditor", AppValues.email);
                            params.put("contactmode", "android");
                            params.put(AppValues.loginAgent,AppValues.userID);
                            params.put("phone", contactViewHolder.phone_edit.getText().toString());
                            params.put("email", contactViewHolder.email_edit.getText().toString());
                            params.put("website", contactViewHolder.web_edit.getText().toString());
                            params.put("facebook", contactViewHolder.facebook_edit.getText().toString());
                            params.put("gplus", contactViewHolder.gplus_edit.getText().toString());
                            params.put("youtube", contactViewHolder.youtube_edit.getText().toString());
                            params.put("wikipedia", contactViewHolder.wikipedia_edit.getText().toString());
                            Log.d(TAG, "from params contact : " + params.toString());
                            return AppValues.checkParams(params);
                        }
                    };


                    AppController.getInstance().addToRequestQueue(stringRequest, "contact");

                }
            });

        }
    }

    public void switchView(TextView textView, EditText editText, String what, boolean change) {
        if (what != null && what.equalsIgnoreCase("Edit")) {
            editText.setText(AppValues.isData(textView.getText().toString()));
        } else {
            if (change) {
                textView.setText(editText.getText());
            }
        }

    }

    @Override
    public int getItemViewType(int position) {
        int new_postion = padDetailList.size();
        if (position == 0) {
            return TYPE_HEADER;
        } else if (position == new_postion + 1) {
            return TYPE_HISTORY;
        } else if (position == new_postion + 2) {
            return TYPE_CONTACT;
        } else return TYPE_PAD;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView lastLocation;
        TextView time;
        TextView samadhiText;


        public HeaderViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            lastLocation = (TextView) itemView.findViewById(R.id.last_location);
            time = (TextView) itemView.findViewById(R.id.time);
            samadhiText = (TextView) itemView.findViewById(R.id.samadhi);
        }
    }

    public class PadDetailViewHolder extends RecyclerView.ViewHolder {
        TextView deekshaTitle;
        TextView name;
        TextView deekshaDate;
        TextView deekshaGuru;
        TextView place;
        RelativeLayout expandedLayout;
        ImageView edit;
        Button cancel, submit;
        EditText dateEdit, placeEdit;
        AutoCompleteTextView guruEdit;
        RelativeLayout editLayout;
        CircleProgressBar progressBar;

        public PadDetailViewHolder(View itemView) {
            super(itemView);
            deekshaTitle = (TextView) itemView.findViewById(R.id.deeksha_title);
            name = (TextView) itemView.findViewById(R.id.name);
            deekshaDate = (TextView) itemView.findViewById(R.id.deeksha_date);
            deekshaGuru = (TextView) itemView.findViewById(R.id.deeksha_guru);
            place = (TextView) itemView.findViewById(R.id.place);
            expandedLayout = (RelativeLayout) itemView.findViewById(R.id.expanded_layout);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            cancel = (Button) itemView.findViewById(R.id.cancel);
            submit = (Button) itemView.findViewById(R.id.submit);
            dateEdit = (EditText) itemView.findViewById(R.id.deeksha_date_edit);
            guruEdit = (AutoCompleteTextView) itemView.findViewById(R.id.deeksha_guru_edit);
            placeEdit = (EditText) itemView.findViewById(R.id.place_edit);
            editLayout = (RelativeLayout) itemView.findViewById(R.id.edit_layout);
            progressBar = (CircleProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout expandedLayout;
        TextView birthNameText;
        TextView birthName;
        TextView dobText;
        TextView dob;
        TextView birthPlaceText;
        TextView birthPlace;
        TextView fatherText;
        TextView father;
        TextView motherText;
        TextView mother;
        TextView educationText;
        TextView education;
        ImageView edit;
        Button cancel, submit;
        RelativeLayout editLayout;
        EditText birthNameEdit, dobEdit, birthPlaceEdit, fatherEdit, motherEdit, educationEdit;
        CircleProgressBar progressBar;


        public HistoryViewHolder(View itemView) {
            super(itemView);
            expandedLayout = (RelativeLayout) itemView.findViewById(R.id.expanded_layout);
            birthName = (TextView) itemView.findViewById(R.id.birthname);
            birthNameText = (TextView) itemView.findViewById(R.id.birthname_text);
            dob = (TextView) itemView.findViewById(R.id.dob);
            dobText = (TextView) itemView.findViewById(R.id.dob_text);
            birthPlace = (TextView) itemView.findViewById(R.id.birthplace);
            birthPlaceText = (TextView) itemView.findViewById(R.id.birthplace_text);
            father = (TextView) itemView.findViewById(R.id.father);
            fatherText = (TextView) itemView.findViewById(R.id.father_text);
            mother = (TextView) itemView.findViewById(R.id.mother);
            motherText = (TextView) itemView.findViewById(R.id.mother_text);
            education = (TextView) itemView.findViewById(R.id.education);
            educationText = (TextView) itemView.findViewById(R.id.education_text);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            cancel = (Button) itemView.findViewById(R.id.cancel);
            submit = (Button) itemView.findViewById(R.id.submit);
            birthNameEdit = (EditText) itemView.findViewById(R.id.birthname_edit);
            dobEdit = (EditText) itemView.findViewById(R.id.dob_edit);
            birthPlaceEdit = (EditText) itemView.findViewById(R.id.birthplace_edit);
            fatherEdit = (EditText) itemView.findViewById(R.id.father_edit);
            motherEdit = (EditText) itemView.findViewById(R.id.mother_edit);
            educationEdit = (EditText) itemView.findViewById(R.id.education_edit);
            editLayout = (RelativeLayout) itemView.findViewById(R.id.edit_layout);
            progressBar = (CircleProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout expandedLayout;
        TextView phone;
        ImageView phone_icon;
        TextView email;
        ImageView email_icon;
        TextView web;
        ImageView web_icon;
        TextView facebook;
        ImageView facebook_icon;
        TextView gplus;
        ImageView gplus_icon;
        TextView youtube;
        ImageView youtube_icon;
        TextView wikipedia;
        ImageView wikipedia_icon;
        ImageView edit;
        Button cancel, submit;
        EditText phone_edit, email_edit, web_edit, facebook_edit, gplus_edit, youtube_edit, wikipedia_edit;
        RelativeLayout editLayout;
        CircleProgressBar progressBar;


        public ContactViewHolder(View itemView) {
            super(itemView);
            expandedLayout = (RelativeLayout) itemView.findViewById(R.id.expanded_layout);
            phone = (TextView) itemView.findViewById(R.id.phone);
            phone_icon = (ImageView) itemView.findViewById(R.id.phone_icon);
            email = (TextView) itemView.findViewById(R.id.email);
            email_icon = (ImageView) itemView.findViewById(R.id.email_icon);
            web = (TextView) itemView.findViewById(R.id.web);
            web_icon = (ImageView) itemView.findViewById(R.id.web_icon);
            facebook = (TextView) itemView.findViewById(R.id.facebook);
            facebook_icon = (ImageView) itemView.findViewById(R.id.facebook_icon);
            gplus = (TextView) itemView.findViewById(R.id.gplus);
            gplus_icon = (ImageView) itemView.findViewById(R.id.gplus_icon);
            youtube = (TextView) itemView.findViewById(R.id.youtube);
            youtube_icon = (ImageView) itemView.findViewById(R.id.youtube_icon);
            wikipedia = (TextView) itemView.findViewById(R.id.wikipedia);
            wikipedia_icon = (ImageView) itemView.findViewById(R.id.wikipedia_icon);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            cancel = (Button) itemView.findViewById(R.id.cancel);
            submit = (Button) itemView.findViewById(R.id.submit);
            phone_edit = (EditText) itemView.findViewById(R.id.phone_edit);
            email_edit = (EditText) itemView.findViewById(R.id.email_edit);
            web_edit = (EditText) itemView.findViewById(R.id.web_edit);
            facebook_edit = (EditText) itemView.findViewById(R.id.facebook_edit);
            gplus_edit = (EditText) itemView.findViewById(R.id.gplus_edit);
            youtube_edit = (EditText) itemView.findViewById(R.id.youtube_edit);
            wikipedia_edit = (EditText) itemView.findViewById(R.id.wikipedia_edit);
            editLayout = (RelativeLayout) itemView.findViewById(R.id.edit_layout);
            progressBar = (CircleProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }

    @Override
    public int getItemCount() {
        if (padDetailList.size() > 0)
            return padDetailList.size() + 3;
        return 0;
    }

    public void setHistory(String str, TextView value, TextView value_text) {
        value.setText(AppValues.checkString(str));
    }

    public void setContact(String str, TextView value, ImageView image) {
        if (str != null && !str.trim().isEmpty() && !str.equalsIgnoreCase("#") && !str.equalsIgnoreCase("N/A")) {
            value.setVisibility(View.VISIBLE);
            value.setText(AppValues.checkString(str));
            image.setVisibility(View.VISIBLE);
        }
    }

    public void setLocation(String address, Double lat, Double lng) {
        if (history) {
            tempHistoryViewHolder.birthPlaceEdit.setText(address);
            tempHistoryViewHolder.birthPlaceEdit.clearFocus();
            latLng = new LatLng(lat, lng);
        } else if (pad) {
            tempPadDetailViewHolder.placeEdit.setText(address);
            tempPadDetailViewHolder.placeEdit.clearFocus();
            latLng = new LatLng(lat, lng);
        }
        history = false;
        pad = false;
        //latLng = null;
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void setDate(String date) {
        if (history) {
            tempHistoryViewHolder.dobEdit.setText(date);
        } else if (pad) {
            tempPadDetailViewHolder.dateEdit.setText(date);
        }
        history = false;
        pad = false;

    }

}
