package com.architjn.jainmunilocator.Activity;

import android.animation.AnimatorSet;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.architjn.jainmunilocator.Adapter.MuniDetailAdapter;
import com.architjn.jainmunilocator.AppController;
import com.architjn.jainmunilocator.AppUtil.AppValues;
import com.architjn.jainmunilocator.Models.CompleteMuniDetail;
import com.architjn.jainmunilocator.Models.ContactModel;
import com.architjn.jainmunilocator.Models.HeaderDetailModel;
import com.architjn.jainmunilocator.Models.HistoryDataModel;
import com.architjn.jainmunilocator.Models.MuniDataType;
import com.architjn.jainmunilocator.Models.PadDetailModel;
import com.architjn.jainmunilocator.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Aman Jain on 23/05/16.
 */
public class MuniDetail extends AppCompatActivity {
    private static final String EXTRA_IMAGE = "com.antonioleiva.materializeyourapp.extraImage";
    private static final String EXTRA_TITLE = "com.antonioleiva.materializeyourapp.extraTitle";
    private static final String TAG = "MUNIDETAIL";
    MuniDataType muniData;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    RecyclerView recyclerView;
    CompleteMuniDetail data = new CompleteMuniDetail();
    List<PadDetailModel> padDetailList = new ArrayList<PadDetailModel>();
    ImageView muniImage;
    MuniDetailAdapter muniDetailAdapter;
    HistoryDataModel histroyData = new HistoryDataModel();
    HeaderDetailModel headerData = new HeaderDetailModel();
    ContactModel contactData = new ContactModel();
    private int CUSTOM_AUTOCOMPLETE_REQUEST_CODE = 13;
    private int MUNI_LOCATION = 15;
    static final int DATE_PICKER_ID = 1111;
    FloatingActionButton fab;
    private ProgressDialog progressDialog;
    private long time = 0;
    private static final int TIME_INTERVAL = 3000;
    private AnimatorSet mCurrentAnimator;
    private int mShortAnimationDuration;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivityTransitions();
        setContentView(R.layout.muni_detail);
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                if (progressDialog.isShowing()) {
                    if (time + TIME_INTERVAL > System.currentTimeMillis()) {
                        progressDialog.dismiss();
                        super.onBackPressed();
                        return;
                    } else {
                        Toast.makeText(getBaseContext(), "Please wait.. or Press again to cancel the ongoing request.", Toast.LENGTH_SHORT).show();
                    }
                    time = System.currentTimeMillis();
                }
            }
        };
        progressDialog.setMessage("Updating Location...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        recyclerView = (RecyclerView) findViewById(R.id.muni_detail_recycler_view);
        muniImage = (ImageView) findViewById(R.id.muni_image);
        muniData = (MuniDataType) getIntent().getSerializableExtra("munidata");
        String imgUrl = "http://jainmunilocator.org/munis/uploads/" + muniData.getUname().toLowerCase() + "_" + muniData.getName().replaceAll(" ", "").toLowerCase() + ".jpg";
        Log.v("img", imgUrl);
        jsonParser(muniData.getId());
        Picasso.with(MuniDetail.this).load(imgUrl).into(muniImage, new AppValues.MarkerCallback(MuniDetail.this, null, muniImage, muniData, 1));
        ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), EXTRA_IMAGE);
        supportPostponeEnterTransition();
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String itemTitle = muniData.getPrefix() + " " + muniData.getName() + " " + muniData.getSuffix();
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(itemTitle);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MuniDetail.this, PickLocationActivity.class);
                i.putExtra("muni", true);
                startActivityForResult(i, MUNI_LOCATION);
                overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top);
            }
        });


        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        muniDetailAdapter = new MuniDetailAdapter(this, data, headerData, padDetailList, histroyData, contactData);
        recyclerView.setAdapter(muniDetailAdapter);


    }


    public void jsonParser(String id) {
        progressDialog.setMessage("Loading Data...");
        progressDialog.show();

        String URL = "http://jainmunilocator.org/munis/muniapi.php?id=" + id;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("DetailFethcer", response);
                        List<CompleteMuniDetail> list = new Gson().fromJson(response, new TypeToken<List<CompleteMuniDetail>>() {
                        }.getType());
                        ArrayList<String> muniName = new ArrayList<>();
                        HashMap<String, Integer> muniNameID = new HashMap<>();
                        data = list.get(0);
                        if (data != null) {
                            if (data.getDos() != null && data.getDos().equalsIgnoreCase("0000-00-00")) {
                                headerData = new HeaderDetailModel("Last Location Detail", AppValues.getName(data), data.getLocation(), data.getTimestamp());
                            } else {
                                fab.setVisibility(View.GONE);
                                headerData = new HeaderDetailModel("Samaadhi Detail", AppValues.getName(data), data.getLocation(), data.getDos());
                            }
                            if (data.getSuffix().contains("Maharaj")) {
                                padDetailList.add(new PadDetailModel("acharya", "Aacharya Pad Detail", data.getAcharyaid(), null, data.getAdate(), data.getAguru(), data.getAplace(), data.getAlat(), data.getAlng()));
                                padDetailList.add(new PadDetailModel("ailacharya", "Ailacharya Pad Detail", data.getAilacharyaid(), data.getAilacharyaname(), data.getAilacharyadate(), data.getAilacharyaguru(), data.getAilacharyaplace(), data.getAilacharyalat(), data.getAilacharyalng()));
                                padDetailList.add(new PadDetailModel("upadhyay", "Upadhyay Pad Detail", data.getUpadhyayid(), data.getUpadhyayname(), data.getUpadhyaydate(), data.getUpadhyayguru(), data.getUpadhyayplace(), data.getUpadhyaylat(), data.getUpadhyaylng()));
                                padDetailList.add(new PadDetailModel("muni", "Muni Pad Detail", data.getMuniid(), data.getMuniname(), data.getMunidate(), data.getMuniguru(), data.getMuniplace(), data.getMunilat(), data.getMunilng()));
                                padDetailList.add(new PadDetailModel("ailak", "Ailak Pad Detail", data.getAilakid(), data.getAilakname(), data.getAilakdate(), data.getAilakguru(), data.getAilakplace(), data.getAilaklat(), data.getAilaklng()));
                                padDetailList.add(new PadDetailModel("kshullak", "Kshullak Pad Detail", data.getKid(), data.getKname(), data.getKdate(), data.getKguru(), data.getKplace(), data.getKlat(), data.getKlng()));
                            } else {
                                padDetailList.add(new PadDetailModel("ganini", "Ganini Pad Detail", data.getGaniniid(), null, data.getGaninidate(), data.getGaniniguru(), data.getGaniniplace(), data.getGaninilat(), data.getGaninilng()));
                                padDetailList.add(new PadDetailModel("aryika", "Aryika Pad Detail", data.getAryikaid(), null, data.getAryikadate(), data.getAryikaguru(), data.getAryikaplace(), data.getAryikalat(), data.getAryikalng()));
                                padDetailList.add(new PadDetailModel("kshullika", "Kshullika Pad Detail", data.getKshullikaid(), null, data.getKshullikadate(), data.getKshullikaguru(), data.getKshullikaplace(), data.getKshullikalat(), data.getKshullikalng()));
                            }
                                padDetailList.add(new PadDetailModel("bhramcharya", "Bhramcharya Pad Detail", data.getBhramcharyaid(), null, data.getBhramcharyadate(), data.getBhramcharyaguru(), data.getBhramcharyaplace(), data.getBhramcharyalat(), data.getBhramcharyalng()));

                                histroyData = new HistoryDataModel(data.getBirthname(), data.getDob(), "", data.getFather(), data.getMother(), data.getKid(), data.getEducation(),data.getBirthLat(),data.getBirthLng());
                                contactData = new ContactModel(data.getPhone(), data.getEmail(), data.getWebsite(), data.getFacebook(), data.getGplus(), data.getYoutube(), data.getWikipedia());
                                muniDetailAdapter = new MuniDetailAdapter(MuniDetail.this, data, headerData, padDetailList, histroyData, contactData);
                                recyclerView.setAdapter(muniDetailAdapter);
                                muniDetailAdapter.notifyDataSetChanged();
                            }
                            if (progressDialog != null) {
                                progressDialog.cancel();
                            }

                        }
                    }

                    ,
                            new Response.ErrorListener()

                    {
                        @Override
                        public void onErrorResponse (VolleyError error){
                        if (progressDialog != null) {
                            progressDialog.cancel();
                        }
                    }
                    }

                    );

                    AppController.getInstance().

                    addToRequestQueue(stringRequest);
                }


    private void initActivityTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide transition = new Slide();
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            getWindow().setEnterTransition(transition);
            getWindow().setReturnTransition(transition);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent intentData) {
        super.onActivityResult(requestCode, resultCode, intentData);

        if (requestCode == CUSTOM_AUTOCOMPLETE_REQUEST_CODE) {
            if (intentData != null) {
                // set the location recieved from picklocation activity
                //txtLocation.setText(data.getStringExtra("Location Address"));
                if (muniDetailAdapter != null) {
                    Log.d(TAG, "onActivityResult: "+intentData.getDoubleExtra("lat", 0.000000)+"   "+ intentData.getDoubleExtra("lng", 0.000000));
                    muniDetailAdapter.setLocation(intentData.getStringExtra("Location Address"), intentData.getDoubleExtra("lat", 0.000000), intentData.getDoubleExtra("lng", 0.000000));
                }

            }
        } else if (requestCode == MUNI_LOCATION) {
            if (intentData != null) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(MuniDetail.this);
                AlertDialog alert;
                builder.setTitle("UPDATE LOCATION");
                builder.setMessage("Is " + AppValues.getName(data) + "\n is at \n" + intentData.getStringExtra("Location Address") + " ?");
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        if (progressDialog != null) {
                            progressDialog.setMessage("Updating Location...");
                            progressDialog.show();
                        }
                        String url = "http://jainmunilocator.org/munis/setloc.php";
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                data.setLocation(intentData.getStringExtra("Location Address"));
                                jsonParser(data.getId());
                                if (progressDialog != null) {
                                    progressDialog.cancel();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (progressDialog != null) {
                                    progressDialog.cancel();
                                }
                                Toast.makeText(MuniDetail.this, "Error Occured. Try Again", Toast.LENGTH_SHORT).show();
                            }
                        }

                        ) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<>();
                                params.put("locid", data.getId());
                                params.put("loceditor", AppValues.email);
                                params.put("locmode", "android");
                                params.put(AppValues.loginAgent,AppValues.userID);
                                params.put("location", intentData.getStringExtra("Location Address"));
                                params.put("locality", intentData.getStringExtra("city"));
                                params.put("lat", String.valueOf(intentData.getDoubleExtra("lat", 0.000000)));
                                params.put("lng", String.valueOf(intentData.getDoubleExtra("lng", 0.000000)));


                                Log.d(TAG, "from params location" + params.toString());
                                return AppValues.checkParams(params);
                            }
                        };
                        AppController.getInstance().addToRequestQueue(stringRequest, "pad");
                    }
                });

                builder.setCancelable(false);
                alert = builder.create();
                alert.show();
            }

        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                final int day = c.get(Calendar.DAY_OF_MONTH);
                // open datepicker dialog.
                // set date picker for current date
                // add pickerListener listner to date picker
                return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (muniDetailAdapter != null) {
                            StringBuilder date = new StringBuilder().append(year)
                                    .append("-")
                                    .append(monthOfYear + 1)
                                    .append("-")
                                    .append(dayOfMonth)
                                    .append(" ");
                            String finalDate = "0000-00-00";
                            SimpleDateFormat myTime = new SimpleDateFormat("yyyy-MM-dd");
                            SimpleDateFormat wantsTime = new SimpleDateFormat("EEEE, dd MMMM yyyy");
                            try {
                                finalDate = wantsTime.format(myTime.parse(date.toString()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            muniDetailAdapter.setDate(finalDate);
                        }
                    }
                }, year, month, day);
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom);
    }
}
