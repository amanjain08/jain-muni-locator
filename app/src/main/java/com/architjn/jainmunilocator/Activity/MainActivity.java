package com.architjn.jainmunilocator.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.architjn.jainmunilocator.Adapter.FilterWithSpaceAdapter;
import com.architjn.jainmunilocator.AppController;
import com.architjn.jainmunilocator.AppUtil.AppValues;
import com.architjn.jainmunilocator.AppUtil.DetailFetcher;
import com.architjn.jainmunilocator.Models.MuniDataType;
import com.architjn.jainmunilocator.Models.MuniMapModel;
import com.architjn.jainmunilocator.MonitorService;
import com.architjn.jainmunilocator.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

import static com.architjn.jainmunilocator.AppUtil.AppValues.getName;
import static com.architjn.jainmunilocator.AppUtil.AppValues.isInternetConnected;

public class MainActivity extends AppCompatActivity implements ClusterManager.OnClusterClickListener<MuniMapModel>, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, SearchView.OnQueryTextListener {

    private static final String TAG = "MainActivity";
    private UiSettings mUiSettings;
    private static final int REQUEST_ID_PERMISSION = 1;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int REQUEST_CODE_LOCATION = 2;
    public static SlidingUpPanelLayout slidingPanelLayout;
    public WebView webView;
    PlaceAutocompleteFragment autocompleteFragment;
    ImageView imageview;
    public static ProgressDialog progressDialog;
    private long time = 0;
    public static CircleProgressBar circleProgressBar;
    private static final int TIME_INTERVAL = 3000;
    public static CoordinatorLayout coordinatorLayout;
    public static HashMap<String, Integer> muniNameID = new HashMap<>();
    public static ArrayList<String> muniName = new ArrayList<>();
    AutoCompleteTextView muniSearch;
    RelativeLayout placeSearch;
    RadioGroup radioGroup;
    RadioButton muniRadioName;
    RadioButton placeRadioName;
    FilterWithSpaceAdapter<String> adapter;
    RelativeLayout muniSearchLayout;
    ImageView muniClearIcon;
    GoogleApiClient mGoogleApiClient;
    LocationRequest locationRequest;
    OwnIconRendered ownIconRendered;
    boolean isconnected = false;
    private ClusterManager<MuniMapModel> mClusterManager;
    private GoogleMap mMap;
    int index = -999;
    private Location mLocation;
    private Marker marker;
    private MuniDataType muniMapData;
    private LatLng prevLatlng;
    public static ArrayList<MuniDataType> muniList;
    boolean clusterclick = true;
    boolean itemclick = false;
    static Snackbar snackbar;
    boolean getLocation = false;
    // FloatingActionButton fab;

    Button feedback, switchView;
    ListView listView;
    SearchView mSearchView;
    TextView backButton;
    View v;
    Filter filter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button button;
        setContentView(R.layout.main_tab);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        // fab = (FloatingActionButton) findViewById(R.id.fab);
        feedback = (Button) findViewById(R.id.feedback);
        listView = (ListView) findViewById(R.id.listView1);
        listView.setTextFilterEnabled(true);
        backButton = (TextView) findViewById(R.id.back_button);
        mSearchView = (SearchView) findViewById(R.id.search_view);
        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                if (progressDialog.isShowing()) {
                    if (time + TIME_INTERVAL > System.currentTimeMillis()) {
                        progressDialog.dismiss();
                        super.onBackPressed();
                        return;
                    } else {
                        Toast.makeText(getBaseContext(), "Please wait.. or Press again to cancel the ongoing request.", Toast.LENGTH_SHORT).show();
                    }
                    time = System.currentTimeMillis();
                }
            }
        };
        progressDialog.setMessage("Loading ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

        //setUpMapIfNeeded();
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
                View promptView = layoutInflater.inflate(R.layout.feedback, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setView(promptView);
                final AlertDialog[] alert = new AlertDialog[1];
                final CircleProgressBar progressBar = (CircleProgressBar) promptView.findViewById(R.id.progressbar_circular_activation);
                Button feedback_submit_button = (Button) promptView.findViewById(R.id.feedback_submit_button);
                final EditText user_feedback_et = (EditText) promptView.findViewById(R.id.user_feedback_et);
                ImageView closeButton = (ImageView) promptView.findViewById(R.id.close_button);
                feedback_submit_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // for good to know dialog
                        final String userfeedback = user_feedback_et.getText().toString();
                        if (!userfeedback.isEmpty()) {
                            progressBar.setVisibility(View.VISIBLE);
                            String url = "http://jainmunilocator.org/mails/feedback.php";
                            StringRequest strReq = new StringRequest(Request.Method.POST, url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            Log.d(TAG, "RESPONSE : " + response);
                                            progressBar.setVisibility(View.GONE);
                                            Toast.makeText(MainActivity.this, "Your feedback has been submitted.", Toast.LENGTH_SHORT).show();
                                            alert[0].cancel();
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            VolleyLog.d(TAG, "Error: " + error.getMessage());
                                            error.printStackTrace();
                                            progressBar.setVisibility(View.GONE);
                                        }
                                    }) {
                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("email", AppValues.email);
                                    params.put("subject", "Feedback from Jain Muni Locator App");
                                    params.put("msg", userfeedback);
                                    params.put("name", AppValues.name);
                                    Log.d(TAG, params.toString());
                                    return AppValues.checkParams(params);
                                }
                            };
                            // Adding request to request queue
                            AppController.getInstance().addToRequestQueue(strReq);

                        } else {
                            Toast.makeText(MainActivity.this, "Please enter feedback", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                alert[0] = alertDialogBuilder.create();
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert[0].cancel();
                    }
                });
                alert[0].show();
            }
        });
        switchView = (Button) findViewById(R.id.list_view);
        switchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // searchView.setIconified(false);
                slidingPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });

        circleProgressBar = (CircleProgressBar) findViewById(R.id.progressBar);
        muniSearch = (AutoCompleteTextView) findViewById(R.id.muni_search);
        placeSearch = (RelativeLayout) findViewById(R.id.place_search);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        muniRadioName = (RadioButton) findViewById(R.id.muni_name);
        muniClearIcon = (ImageView) findViewById(R.id.muni_clear_icon);
        muniSearchLayout = (RelativeLayout) findViewById(R.id.muni_search_layout);
        placeRadioName = (RadioButton) findViewById(R.id.place_name);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.muni_name) {
                    muniSearchLayout.setVisibility(View.VISIBLE);
                    placeSearch.setVisibility(View.GONE);
                } else {
                    muniSearchLayout.setVisibility(View.GONE);
                    placeSearch.setVisibility(View.VISIBLE);
                }
            }
        });
        muniSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                index = muniNameID.get(parent.getItemAtPosition(position));
                searchMuni(index);
                Log.d(TAG, "onItemClick: Index : " + index + "   name : " + muniName.get(index) + " anothrt : " + AppValues.getName(muniList.get(index)));
                muniSearch.clearFocus();

            }
        });

        slidingPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_panel_layout);
        slidingPanelLayout.setDragView(backButton);
        slidingPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        SlideListener slideListener = new SlideListener();
        slidingPanelLayout.addPanelSlideListener(slideListener);

        autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setHint("Search for a place");
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                search(place);
            }

            @Override
            public void onError(Status status) {
                Log.d(TAG, "onError: " + status.getStatusMessage());

            }
        });


        //webView = (WebView) findViewById(R.id.web_view);
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Intent serviceIntent = new Intent(AppController.getContext(), MonitorService.class);
                startService(serviceIntent);
            }
        };
        handler.postDelayed(runnable, 7500);
        setupSearchView();
        checkAndRequestPermissions();


    }

    private void setupSearchView() {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
    }

    @Override
    public boolean onClusterClick(Cluster<MuniMapModel> cluster) {

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cluster.getPosition(), mMap.getCameraPosition().zoom + 2f), 1000, null);
        int i = 0;
        for (MuniDataType temp : muniList) {
            LatLng latLng = new LatLng(temp.getLat(), temp.getLon());
            Log.d(TAG, "onClusterClick: Lat : " + temp.getLat() + "  Long : " + temp.getLon());
            if (cluster.getPosition().equals(latLng)) {
                clusterclick = false;
                break;
            }
            i++;
        }
        if (!clusterclick) {
            // viewPager_map.setCurrentItem(i, true);
            muniMapData = muniList.get(i);
        }
        //Log.d("CAmera zoom", "cluster click");
        return true;
    }


    private void setUpMapIfNeeded() {

        if (mMap != null) {
            return;
        }


        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapview)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                if (mMap != null) {
                    mUiSettings = mMap.getUiSettings();
                    mUiSettings.setZoomControlsEnabled(true);
                    mUiSettings.setZoomGesturesEnabled(true);
                    mUiSettings.setScrollGesturesEnabled(true);

                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.

                    } else {
                        mMap.setMyLocationEnabled(true);
                    }

                    mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                        @Override
                        public void onCameraMove() {
                            Log.d("CAMERA POSITION ", "ZOOM LEVEL : " + mMap.getCameraPosition());
                            if (mMap.getCameraPosition().zoom < 15.5f && !itemclick) {
                                if (marker != null) {
                                    marker.hideInfoWindow();
                                }
                            } else {
                                itemclick = false;
                            }


                        }
                    });


                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {

                            MuniDataType muni = muniList.get(Integer.parseInt(marker.getSnippet()));
                            Intent intent = new Intent(MainActivity.this, MuniDetail.class);
                            intent.putExtra("munidata", muni);
                            startActivity(intent);
                            overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top);


                        }
                    });

                    startDemo();

                }
            }
        });

    }

    private boolean checkAndRequestPermissions() {
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_PERMISSION);
            return false;
        }
        fetchMunidetail();
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.d(TAG, "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_PERMISSION: {
                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "location services permission granted");
                        fetchMunidetail();
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d(TAG, "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                            showDialogOK();
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            //   Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG).show();
                            Snackbar snackbar = Snackbar.make(coordinatorLayout, "App needs location permission to access this feature.", Snackbar.LENGTH_INDEFINITE);
                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.WHITE);
                            snackbar.setActionTextColor(Color.RED);
                            snackbar.setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showDialogOK();
                                }
                            });
                            snackbar.show();
                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Location Services Permission is required for this App.\n" +
                "Do you want to try again ?");
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Snackbar snackbar = Snackbar.make(coordinatorLayout, "App needs location permission to access this feature.", Snackbar.LENGTH_INDEFINITE);
                View view = snackbar.getView();
                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.WHITE);
                snackbar.setActionTextColor(Color.RED);
                snackbar.setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkAndRequestPermissions();
                    }
                });
                snackbar.show();
            }
        });

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                checkAndRequestPermissions();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                Snackbar snackbar = Snackbar.make(coordinatorLayout, "App needs location permission to access this feature.", Snackbar.LENGTH_INDEFINITE);
                View view = snackbar.getView();
                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.WHITE);
                snackbar.setActionTextColor(Color.RED);
                snackbar.setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkAndRequestPermissions();
                    }
                });
                snackbar.show();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    protected void startDemo() {


        mClusterManager = new ClusterManager<MuniMapModel>(this, mMap);
        ownIconRendered = new OwnIconRendered();
        mClusterManager.setRenderer(new MyClusterRenderer(this, mMap, mClusterManager));
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new MyCustomAdapterForItems());
        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<MuniMapModel>() {
            @Override
            public boolean onClusterItemClick(MuniMapModel item) {
                muniMapData = item.getMuniDataType();
                prevLatlng = item.getPosition();
                if (mMap.getCameraPosition().zoom < 18f) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(prevLatlng, 18f), 1200, null);
                } else {
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(prevLatlng), 1200, null);
                }
                itemclick = true;
                return false;
            }
        });
        addItems(muniList);


        // mClusterManager.setOOLnClusterItemInfoWindowClickListener(this);
    }


    @Override
    protected void onStart() {
        Log.d(TAG, "onstart called");

        super.onStart();

    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onresume called");

        if (muniList != null && muniList.size() < 0) {
            fetchMunidetail();
        }

        super.onResume();

    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onpause called");

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            isconnected = false;
            SmartLocation.with(this).location().stop();
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onstop called");

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    protected synchronized void buildGoogleApiClient() {
        //mCircularProgressBar.setVisibility(View.VISIBLE);
        if (isInternetConnected(MainActivity.this)) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API).build();
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d("OnConnected", "Connection successful");
        isconnected = true;
        settingsrequest();

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    public void settingsrequest() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            getLocation = false;
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setNumUpdates(1);
            locationRequest.setExpirationDuration(20000);
            locationRequest.setFastestInterval(500);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.setAlwaysShow(true); //this is the key ingredient
            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS: {
                            setLocation();
                        }
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        } else {
            getLocation = true;
            if (mGoogleApiClient == null)
                buildGoogleApiClient();
            else if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        setLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        if (progressDialog != null) {
                            progressDialog.cancel();
                        }
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "App needs location to access this feature.", Snackbar.LENGTH_INDEFINITE);
                        View view = snackbar.getView();
                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.WHITE);
                        snackbar.setActionTextColor(Color.RED);
                        snackbar.setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                settingsrequest();
                            }
                        });
                        snackbar.show();
                        //settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        mClusterManager = null;
        mMap = null;
        super.onDestroy();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            if (filter != null)
                filter.filter(null);
        } else {
            filter.filter(newText);
        }

        return true;
    }

    public class OwnIconRendered extends DefaultClusterRenderer<MuniMapModel> {
        public OwnIconRendered() {
            super(getApplicationContext(), mMap, mClusterManager);
        }

        @Override
        protected void onClusterItemRendered(MuniMapModel clusterItem, Marker marker) {

            marker.setSnippet(String.valueOf(clusterItem.getId()));
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.map_marker);
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 80, 80, false);
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(resizedBitmap));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster)

        {
            // Always render clusters.

            return cluster.getSize() > 4;
        }


    }

    public class MyClusterRenderer extends DefaultClusterRenderer<MuniMapModel> {

        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());

        public MyClusterRenderer(Context context, GoogleMap map, ClusterManager<MuniMapModel> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected void onClusterItemRendered(MuniMapModel clusterItem, Marker marker) {
            marker.setSnippet(String.valueOf(clusterItem.getId()));
        }

    }

    @Override
    public void onBackPressed() {

        if (slidingPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            slidingPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            return;
        }
        super.onBackPressed();
        overridePendingTransition(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom);
    }


    class SlideListener implements SlidingUpPanelLayout.PanelSlideListener {

        @Override
        public void onPanelSlide(View panel, float slideOffset) {
            // Log.d("SLide Listener", slideOffset + "");

        }

        @Override
        public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
            if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                slidingPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }

        }
    }

    private void addItems(ArrayList<MuniDataType> muniList) {
        int i = 0;
        int j = 0;
        if (muniList != null) {
            j = muniList.size();
        }

        Log.d("number of item", "items : " + j);
        for (MuniDataType data : muniList) {
            if (data.getDos() == null || data.getDos().equalsIgnoreCase("0000-00-00")) {
                LatLng latLng = new LatLng(data.getLat(), data.getLon());
                mClusterManager.addItem(new MuniMapModel(data.getTitle(), latLng, i, data));
            }
            i++;

        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(21.0000, 78.0000), 5f), 1000, null);
        Log.d(TAG, "LENGTH : " + i);


    }

    public void searchMuni(int position) {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        muniMapData = muniList.get(position);
        LatLng latLng = new LatLng((muniList.get(position).getLat()), (muniList.get(position).getLon()));
      /*  if (marker != null) {
            //  marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
            marker.remove();
        }*/
        //  prevLatlng = new LatLng(tourDataTypeModels.get(position).getLat(), tourDataTypeModels.get(position).getLon());
        if (mMap.getCameraPosition().zoom < 18f) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18f), 1200, null);
        } else {
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng), 1200, null);
        }
        /*Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.map_marker);
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 80, 80, false);
        marker = mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap)));
        marker.showInfoWindow();*/
        mClusterManager.onMarkerClick(marker);
    }

    public void search(Place place) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 13f), 1000, null);
    }

    public void setLocation() {
        SmartLocation.with(this).location().oneFix().start(new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {
                if (location == null) {
                    mLocation = location;
                }
                if (progressDialog != null) {
                    progressDialog.cancel();
                }
                SmartLocation.with(MainActivity.this).location().stop();
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 10f), 1000, null);
                mMap.getMyLocation();
            }
        });
    }

    public void showProgress() {
        if (circleProgressBar != null) {
            circleProgressBar.setVisibility(View.VISIBLE);
        }

    }

    public void hideProgress() {
        if (circleProgressBar != null) {
            circleProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    public void fetchMunidetail() {
        if (isInternetConnected(MainActivity.this)) {
            if (progressDialog != null)
                progressDialog.show();
            System.setProperty("http.keepAlive", "false");
            HttpClient httpclient = new DefaultHttpClient();
            httpclient.getConnectionManager().closeExpiredConnections();
            new DetailFetcher(getResources().getString(R.string.fetch_url)) {
                @Override
                public void onDetailsFetched(ArrayList<MuniDataType> muniDataList, ArrayList<String> muniName, final HashMap<String, Integer> muniNameID) {
                    MainActivity.muniName.clear();
                    MainActivity.muniName.addAll(muniName);
                    MainActivity.muniNameID.clear();
                    MainActivity.muniNameID.putAll(muniNameID);
                    FilterWithSpaceAdapter listViewAdapter = new FilterWithSpaceAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, (android.R.id.text1), muniName);
                    listView.setAdapter(listViewAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            hideKeyboard(view);
                            index = muniNameID.get(parent.getItemAtPosition(position));
                            MuniDataType muni = muniList.get(index);
                            Intent intent = new Intent(MainActivity.this, MuniDetail.class);
                            intent.putExtra("munidata", muni);
                            startActivity(intent);
                            mSearchView.setQuery("", false);
                            mSearchView.clearFocus();
                            overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top);
                        }
                    });
                    adapter = new FilterWithSpaceAdapter<String>(MainActivity.this, R.layout.list_item, R.id.suggestion_text, muniName);
                    muniSearch.setAdapter(adapter);

                    filter = listViewAdapter.getFilter();
                    muniList = muniDataList;
                    buildGoogleApiClient();
                    setUpMapIfNeeded();
                }

                @Override
                public void onDetailError() {
                    Log.d(TAG, "server error: called");
                    snackbar = Snackbar.make(coordinatorLayout, "Error Occured.", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            fetchMunidetail();
                        }
                    });
                    snackbar.setActionTextColor(Color.RED);
                    snackbar.show();

                }
            }.execute();
        } else {
            Log.d(TAG, "internt error: called");
            snackbar = Snackbar.make(coordinatorLayout, "No Internet Found.", Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("Retry", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fetchMunidetail();
                }
            });
            snackbar.setActionTextColor(Color.RED);
            snackbar.show();
        }

    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter {

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            if (!AppValues.isGettingImage) {
                if (marker.isInfoWindowShown()) {
                    marker.hideInfoWindow();
                    return null;
                }
                MuniDataType muni = muniMapData;
                if (muni == null) {
                    muni = muniList.get(Integer.parseInt(marker.getSnippet()));
                }
                if (mMap.getCameraPosition().zoom < 18f) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(prevLatlng, 18f), 1200, null);
                } else {
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(prevLatlng), 1200, null);
                }

                v = getLayoutInflater().inflate(R.layout.windowlayout, null);
                ImageView img = (ImageView) v.findViewById(R.id.img);
                TextView name = (TextView) v.findViewById(R.id.name);
                TextView loc = (TextView) v.findViewById(R.id.area);

                String imgUrl = "http://jainmunilocator.org/munis/uploads/" + muni.getUname().toLowerCase() + "_" + muni.getName().replaceAll(" ", "").toLowerCase() + ".jpg";
                Log.v("img", imgUrl);
                {
                    Picasso.with(MainActivity.this).load(imgUrl).into(img, new AppValues.MarkerCallback(MainActivity.this, marker, img, muni, 1));
                }
                name.setText(getName(muni));
                if (muni.getLocation().isEmpty() || muni.getLocation().equalsIgnoreCase("N/A"))
                    loc.setText("Not Available");
                else {
                    loc.setText(muni.getLocation());
                }
            } else {
                AppValues.isGettingImage = false;
            }
            return v;


        }
    }


}
