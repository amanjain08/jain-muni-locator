package com.architjn.jainmunilocator.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.architjn.jainmunilocator.AppController;
import com.architjn.jainmunilocator.AppUtil.AppValues;
import com.architjn.jainmunilocator.R;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;

/**
 * Created by Aman Jain on 28/03/16.
 */
public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private final String TAG = "LoginActivity";
    LoginButton loginButton;
    CallbackManager callbackManager;
    AccessTokenTracker accessTokenTracker;
    AccessToken accessToken;
    ProfileTracker profileTracker;
    GoogleApiClient mGoogleApiClient;
    Button signInButton;
    Button fbSignInButton;
    String personName;
    String personID;
    String email;
    String personPhotoUrl;
    CircleProgressBar circleProgressBar;
    SharedPreferences sharedPreferences;
    private int RC_SIGN_IN = 100;
    boolean signout = false;
    String loginAgent;
    CoordinatorLayout coordinatorLayout;
    private MixpanelAPI mixpanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login_screen);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        AppValues.checkFirstRun(this);
        // setMixpanel();
        AppValues.getInstance().getData(LoginActivity.this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_ME))
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addScope(new Scope(Scopes.PLUS_ME))
                .addScope(new Scope(Scopes.PLUS_LOGIN))
                .build();

        fbSignInButton = (Button) findViewById(R.id.facebook_signin);
        fbSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppValues.isInternetConnected(LoginActivity.this)) {

                    showProgress();
                    loginButton.performClick();
                } else {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "No Internet Found", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });
        signInButton = (Button) findViewById(R.id.google_signin);
        signInButton.setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "user_friends", "email"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {

                accessToken = loginResult.getAccessToken();
                Log.d(TAG, accessToken.getToken() + " " + accessToken.getUserId());
                AccessToken accessToken = loginResult.getAccessToken();
                final AccessToken accessToken1 = loginResult.getAccessToken();
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    Log.v(TAG, response.toString());
                                    personName = object.optString("name");
                                    personID = object.optString("id");
                                    email = object.optString("email");
                                    personPhotoUrl = "https://graph.facebook.com/me/picture?type=large&method=GET&access_token=" + loginResult.getAccessToken().getToken();
                                    Profile profile = Profile.getCurrentProfile();

                                    if (profile != null) {
                                        personName = profile.getName();
                                        personPhotoUrl = "" + profile.getProfilePictureUri(200, 200);
                                        personID = profile.getId();
                                    }
                                    loginAgent = "facebook";
                                    String fbAccessToken = accessToken1.getToken();
                                    sendUserDetail(personName, email, personPhotoUrl, personID, "fbid");
                                    Log.d("Facebook_name", personName + "access token   " + accessToken1.getToken() + "   personID  " + personID + "   photo url  " + personPhotoUrl);
                                } catch (Exception e) {
                                    hideProgress();
                                    e.printStackTrace();
                                    response.getError();
                                    LoginManager.getInstance().logOut();
                                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Login Failed. Please Retry", Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,email,name,picture");
                request.setParameters(parameters);
                request.executeAsync();


            }

            @Override
            public void onCancel() {
                if (circleProgressBar.getVisibility() == View.VISIBLE) {
                    circleProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
                if (circleProgressBar.getVisibility() == View.VISIBLE) {
                    circleProgressBar.setVisibility(View.GONE);
                }
            }
        });
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

            }
        };
        accessToken = AccessToken.getCurrentAccessToken();

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

            }
        };

        circleProgressBar = (CircleProgressBar) findViewById(R.id.progressBar);
        sharedPreferences = getSharedPreferences("LOGIN_DATA", Context.MODE_PRIVATE);

        if (AppValues.isLoggedIn) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top);
            finish();
        }


    }

    private void signIn() {
        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                // ...
                            }
                        });
            }
        }
    }


    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            loginAgent = "google";
            GoogleSignInAccount acct = result.getSignInAccount();
            // mixpanel.identify(AppValues.getDeviceID(LoginActivity.this));
            //mixpanel.getPeople().set("$first_name",acct.getDisplayName());
            // mixpanel.getPeople().set("$created",new Date());
            sendUserDetail(acct.getDisplayName(), acct.getEmail(), String.valueOf(acct.getPhotoUrl()), acct.getId(), "googleid");
            // Toast.makeText(LoginActivity.this, acct.getDisplayName() + ", " + acct.getEmail() + ", " + acct.getPhotoUrl(), Toast.LENGTH_LONG).show();
            Log.d(TAG, acct.getDisplayName() + ", " + acct.getEmail() + ", " + acct.getId() + ", " + acct.getPhotoUrl());
        } else {
            //If login fails
            hideProgress();
            Log.d(TAG, "handleSignInResult: " + result.getStatus().getStatusMessage());
            // Toast.makeText(this, result.getStatus().getStatusMessage() + "   Login Failed", Toast.LENGTH_LONG).show();
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Login Failed. Please Retry", Snackbar.LENGTH_SHORT);


            snackbar.show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        {
            //Calling signin
            // circleProgressBar.setVisibility(View.VISIBLE);
            if (AppValues.isInternetConnected(LoginActivity.this)) {
                showProgress();
                signIn();
            } else {
                Snackbar snackbar = Snackbar.make(coordinatorLayout, "No Internet Found", Snackbar.LENGTH_SHORT);
                snackbar.show();

            }


        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (circleProgressBar.getVisibility() == View.VISIBLE) {
            circleProgressBar.setVisibility(View.GONE);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {

            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void sendUserDetail(final String accntName, final String email, final String photoUrl, final String personID, final String agent) {

        String URL = "http://jainmunilocator.org/login/updateLogin.php?email=" + email.replaceAll(" ", "%20") + "&username=" + accntName.replaceAll(" ", "%20") + "&userimg=" + photoUrl + "&userid=" + personID;
        Log.d(TAG, "URL : " + URL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        {
                            Toast.makeText(LoginActivity.this, "Successfully Logged In", Toast.LENGTH_SHORT).show();
                            AppValues.isLoggedIn = true;
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("name", accntName);
                            editor.putString("email", email);
                            editor.putString("img", photoUrl);
                            editor.putString("id", personID);
                            editor.putBoolean("login", true);
                            editor.putString("agent", agent);
                            editor.apply();
                            AppValues.getInstance().getData(LoginActivity.this);
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top);
                            finish();
                        }
                        hideProgress();
                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "onErrorResponse: " + error.getStackTrace());
                        Log.d(TAG, "onErrorResponse: " + error.getMessage());
                        error.getStackTrace();
                        hideProgress();
                        if (loginAgent.contains("google")) {
                            signOut();
                        } else if (loginAgent.contains("facebook")) {
                            LoginManager.getInstance().logOut();
                        }
                        AppValues.isLoggedIn = false;
                        // Toast.makeText(LoginActivity.this, "Error Occured. Please try again.", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Login Failed.   Please Retry.", Snackbar.LENGTH_SHORT);


                        snackbar.show();
                    }
                });

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void setMixpanel() {

        String deviceId = AppValues.getDeviceID(this);
        String id = deviceId;
        JSONObject props = new JSONObject();
        // Mixpanel
        mixpanel = MixpanelAPI.getInstance(LoginActivity.this, AppValues.MIXPANEL_TOKEN);
        if (!AppValues.isFirstLaunch) {
            mixpanel.track("Launched app", null);
            mixpanel.getPeople().identify(AppValues.getMixPanelUniqueUserId(LoginActivity.this));
            mixpanel.getPeople().increment("No of sessions", 1);
            mixpanel.identify(id);
            mixpanel.registerSuperProperties(props);

        } else {

            AppValues.setMixPanelUniqueUserId(LoginActivity.this, id);
            mixpanel.track("First Launch", props);
            mixpanel.track("Launched app", null);
            mixpanel.registerSuperProperties(props);
            mixpanel.identify(id); // user is created
            mixpanel.getPeople().identify(id);
            mixpanel.getPeople().set("First launch date", Calendar.getInstance().getTime().toString());
            Log.d(TAG, "setMixpanel: For First Time");

        }

    }

    public void showProgress() {
        if (circleProgressBar != null) {
            circleProgressBar.setVisibility(View.VISIBLE);
        }

    }

    public void hideProgress() {
        if (circleProgressBar != null) {
            circleProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom);
    }
}
