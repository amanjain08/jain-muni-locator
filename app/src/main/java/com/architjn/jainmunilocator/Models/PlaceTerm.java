package com.architjn.jainmunilocator.Models;

/**
 * Created by audiocompass on 02/08/16.
 */
public class PlaceTerm {
    private int offset;
    private String value;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
