package com.architjn.jainmunilocator.Models;

/**
 * Created by audiocompass on 29/05/16.
 */
public class ContactModel {
    protected String phone;
    protected String email;
    protected String website;
    protected String facebook;
    protected String gplus;
    protected String youtube;
    protected String wikipedia;

    public ContactModel(String phone, String email, String website, String facebook, String gplus, String youtube, String wikipedia) {
        this.phone = phone;
        this.email = email;
        this.website = website;
        this.facebook = facebook;
        this.gplus = gplus;
        this.youtube = youtube;
        this.wikipedia = wikipedia;
    }

    public ContactModel() {

    }

    public String getPhone() {
        if(phone.equalsIgnoreCase("0"))
        return "Not Available";
        return  phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {

        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getGplus() {
        return gplus;
    }

    public void setGplus(String gplus) {
        this.gplus = gplus;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getWikipedia() {
        return wikipedia;
    }

    public void setWikipedia(String wikipedia) {
        this.wikipedia = wikipedia;
    }
}
