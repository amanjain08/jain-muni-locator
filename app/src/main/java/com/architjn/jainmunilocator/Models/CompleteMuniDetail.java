package com.architjn.jainmunilocator.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CompleteMuniDetail {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("upadhi")
    @Expose
    private String upadhi;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("alias")
    @Expose
    private String alias;
    @SerializedName("vairagya")
    @Expose
    private String vairagya;
    @SerializedName("birthname")
    @Expose
    private String birthname;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("father")
    @Expose
    private String father;
    @SerializedName("mother")
    @Expose
    private String mother;
    @SerializedName("spouse")
    @Expose
    private String spouse;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("approved")
    @Expose
    private String approved;
    @SerializedName("dos")
    @Expose
    private String dos;
    @SerializedName("grahtyag")
    @Expose
    private String grahtyag;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("uname")
    @Expose
    private String uname;
    @SerializedName("prefix")
    @Expose
    private String prefix;
    @SerializedName("suffix")
    @Expose
    private String suffix;
    @SerializedName("acharyaid")
    @Expose
    private String acharyaid;
    @SerializedName("adate")
    @Expose
    private String adate;
    @SerializedName("aguru")
    @Expose
    private String aguru;
    @SerializedName("alat")
    @Expose
    private String alat;
    @SerializedName("alng")
    @Expose
    private String alng;
    @SerializedName("aplace")
    @Expose
    private String aplace;
    @SerializedName("ailacharyaid")
    @Expose
    private String ailacharyaid;
    @SerializedName("ailacharyaname")
    @Expose
    private String ailacharyaname;
    @SerializedName("ailacharyadate")
    @Expose
    private String ailacharyadate;
    @SerializedName("ailacharyaguru")
    @Expose
    private String ailacharyaguru;
    @SerializedName("ailacharyalat")
    @Expose
    private String ailacharyalat;
    @SerializedName("ailacharyalng")
    @Expose
    private String ailacharyalng;
    @SerializedName("ailacharyaplace")
    @Expose
    private String ailacharyaplace;
    @SerializedName("upadhyayid")
    @Expose
    private String upadhyayid;
    @SerializedName("upadhyayname")
    @Expose
    private String upadhyayname;
    @SerializedName("upadhyaydate")
    @Expose
    private String upadhyaydate;
    @SerializedName("upadhyayguru")
    @Expose
    private String upadhyayguru;
    @SerializedName("upadhyaylat")
    @Expose
    private String upadhyaylat;
    @SerializedName("upadhyaylng")
    @Expose
    private String upadhyaylng;
    @SerializedName("upadhyayplace")
    @Expose
    private String upadhyayplace;
    @SerializedName("muniid")
    @Expose
    private String muniid;
    @SerializedName("muniname")
    @Expose
    private String muniname;
    @SerializedName("munidate")
    @Expose
    private String munidate;
    @SerializedName("muniguru")
    @Expose
    private String muniguru;
    @SerializedName("munilat")
    @Expose
    private String munilat;
    @SerializedName("munilng")
    @Expose
    private String munilng;
    @SerializedName("muniplace")
    @Expose
    private String muniplace;
    @SerializedName("ailakid")
    @Expose
    private String ailakid;
    @SerializedName("ailakname")
    @Expose
    private String ailakname;
    @SerializedName("ailakdate")
    @Expose
    private String ailakdate;
    @SerializedName("ailakguru")
    @Expose
    private String ailakguru;
    @SerializedName("ailaklat")
    @Expose
    private String ailaklat;
    @SerializedName("ailaklng")
    @Expose
    private String ailaklng;
    @SerializedName("ailakplace")
    @Expose
    private String ailakplace;
    @SerializedName("kid")
    @Expose
    private String kid;
    @SerializedName("kname")
    @Expose
    private String kname;
    @SerializedName("kdate")
    @Expose
    private String kdate;
    @SerializedName("kguru")
    @Expose
    private String kguru;
    @SerializedName("klat")
    @Expose
    private String klat;
    @SerializedName("klng")
    @Expose
    private String klng;
    @SerializedName("kplace")
    @Expose
    private String kplace;
    @SerializedName("ganiniid")
    @Expose
    private String ganiniid;
    @SerializedName("ganinidate")
    @Expose
    private String ganinidate;
    @SerializedName("ganiniguru")
    @Expose
    private String ganiniguru;
    @SerializedName("ganinilat")
    @Expose
    private String ganinilat;
    @SerializedName("ganinilng")
    @Expose
    private String ganinilng;
    @SerializedName("ganiniplace")
    @Expose
    private String ganiniplace;
    @SerializedName("aryikaid")
    @Expose
    private String aryikaid;
    @SerializedName("aryikadate")
    @Expose
    private String aryikadate;
    @SerializedName("aryikaguru")
    @Expose
    private String aryikaguru;
    @SerializedName("aryikalat")
    @Expose
    private String aryikalat;
    @SerializedName("aryikalng")
    @Expose
    private String aryikalng;
    @SerializedName("aryikaplace")
    @Expose
    private String aryikaplace;
    @SerializedName("kshullikaid")
    @Expose
    private String kshullikaid;
    @SerializedName("kshullikadate")
    @Expose
    private String kshullikadate;
    @SerializedName("kshullikaguru")
    @Expose
    private String kshullikaguru;
    @SerializedName("kshullikalat")
    @Expose
    private String kshullikalat;
    @SerializedName("kshullikalng")
    @Expose
    private String kshullikalng;
    @SerializedName("kshullikaplace")
    @Expose
    private String kshullikaplace;
    @SerializedName("bhramcharyaid")
    @Expose
    private String bhramcharyaid;
    @SerializedName("bhramcharyadate")
    @Expose
    private String bhramcharyadate;
    @SerializedName("bhramcharyaguru")
    @Expose
    private String bhramcharyaguru;
    @SerializedName("bhramcharyalat")
    @Expose
    private String bhramcharyalat;
    @SerializedName("bhramcharyalng")
    @Expose
    private String bhramcharyalng;
    @SerializedName("bhramcharyaplace")
    @Expose
    private String bhramcharyaplace;
    @SerializedName("mid")
    @Expose
    private String mid;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("locality")
    @Expose
    private String locality;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("contactid")
    @Expose
    private String contactid;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("gplus")
    @Expose
    private String gplus;
    @SerializedName("youtube")
    @Expose
    private String youtube;
    @SerializedName("wikipedia")
    @Expose
    private String wikipedia;
    @SerializedName("birthlat")
    private String birthLat;
    @SerializedName("birthlng")
    private String birthLng;

    public String getBirthLat() {
        return birthLat;
    }

    public void setBirthLat(String birthLat) {
        this.birthLat = birthLat;
    }

    public String getBirthLng() {
        return birthLng;
    }

    public void setBirthLng(String birthLng) {
        this.birthLng = birthLng;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The upadhi
     */
    public String getUpadhi() {
        return upadhi;
    }

    /**
     * @param upadhi The upadhi
     */
    public void setUpadhi(String upadhi) {
        this.upadhi = upadhi;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * @param alias The alias
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * @return The vairagya
     */
    public String getVairagya() {
        return vairagya;
    }

    /**
     * @param vairagya The vairagya
     */
    public void setVairagya(String vairagya) {
        this.vairagya = vairagya;
    }

    /**
     * @return The birthname
     */
    public String getBirthname() {
        return birthname;
    }

    /**
     * @param birthname The birthname
     */
    public void setBirthname(String birthname) {
        this.birthname = birthname;
    }

    /**
     * @return The dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob The dob
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return The father
     */
    public String getFather() {
        return father;
    }

    /**
     * @param father The father
     */
    public void setFather(String father) {
        this.father = father;
    }

    /**
     * @return The mother
     */
    public String getMother() {
        return mother;
    }

    /**
     * @param mother The mother
     */
    public void setMother(String mother) {
        this.mother = mother;
    }

    /**
     * @return The spouse
     */
    public String getSpouse() {
        return spouse;
    }

    /**
     * @param spouse The spouse
     */
    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    /**
     * @return The img
     */
    public String getImg() {
        return img;
    }

    /**
     * @param img The img
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * @return The approved
     */
    public String getApproved() {
        return approved;
    }

    /**
     * @param approved The approved
     */
    public void setApproved(String approved) {
        this.approved = approved;
    }

    /**
     * @return The dos
     */
    public String getDos() {
        return dos;
    }

    /**
     * @param dos The dos
     */
    public void setDos(String dos) {
        this.dos = dos;
    }

    /**
     * @return The grahtyag
     */
    public String getGrahtyag() {
        return grahtyag;
    }

    /**
     * @param grahtyag The grahtyag
     */
    public void setGrahtyag(String grahtyag) {
        this.grahtyag = grahtyag;
    }

    /**
     * @return The education
     */
    public String getEducation() {
        return education;
    }

    /**
     * @param education The education
     */
    public void setEducation(String education) {
        this.education = education;
    }

    /**
     * @return The uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid The uid
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * @return The uname
     */
    public String getUname() {
        return uname;
    }

    /**
     * @param uname The uname
     */
    public void setUname(String uname) {
        this.uname = uname;
    }

    /**
     * @return The prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * @param prefix The prefix
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * @return The suffix
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * @param suffix The suffix
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    /**
     * @return The acharyaid
     */
    public String getAcharyaid() {
        return acharyaid;
    }

    /**
     * @param acharyaid The acharyaid
     */
    public void setAcharyaid(String acharyaid) {
        this.acharyaid = acharyaid;
    }

    /**
     * @return The adate
     */
    public String getAdate() {
        return adate;
    }

    /**
     * @param adate The adate
     */
    public void setAdate(String adate) {
        this.adate = adate;
    }

    /**
     * @return The aguru
     */
    public String getAguru() {
        return aguru;
    }

    /**
     * @param aguru The aguru
     */
    public void setAguru(String aguru) {
        this.aguru = aguru;
    }

    /**
     * @return The alat
     */
    public String getAlat() {
        return alat;
    }

    /**
     * @param alat The alat
     */
    public void setAlat(String alat) {
        this.alat = alat;
    }

    /**
     * @return The alng
     */
    public String getAlng() {
        return alng;
    }

    /**
     * @param alng The alng
     */
    public void setAlng(String alng) {
        this.alng = alng;
    }

    /**
     * @return The aplace
     */
    public String getAplace() {
        return aplace;
    }

    /**
     * @param aplace The aplace
     */
    public void setAplace(String aplace) {
        this.aplace = aplace;
    }

    /**
     * @return The ailacharyaid
     */
    public String getAilacharyaid() {
        return ailacharyaid;
    }

    /**
     * @param ailacharyaid The ailacharyaid
     */
    public void setAilacharyaid(String ailacharyaid) {
        this.ailacharyaid = ailacharyaid;
    }

    /**
     * @return The ailacharyaname
     */
    public String getAilacharyaname() {
        return ailacharyaname;
    }

    /**
     * @param ailacharyaname The ailacharyaname
     */
    public void setAilacharyaname(String ailacharyaname) {
        this.ailacharyaname = ailacharyaname;
    }

    /**
     * @return The ailacharyadate
     */
    public String getAilacharyadate() {
        return ailacharyadate;
    }

    /**
     * @param ailacharyadate The ailacharyadate
     */
    public void setAilacharyadate(String ailacharyadate) {
        this.ailacharyadate = ailacharyadate;
    }

    /**
     * @return The ailacharyaguru
     */
    public String getAilacharyaguru() {
        return ailacharyaguru;
    }

    /**
     * @param ailacharyaguru The ailacharyaguru
     */
    public void setAilacharyaguru(String ailacharyaguru) {
        this.ailacharyaguru = ailacharyaguru;
    }

    /**
     * @return The ailacharyalat
     */
    public String getAilacharyalat() {
        return ailacharyalat;
    }

    /**
     * @param ailacharyalat The ailacharyalat
     */
    public void setAilacharyalat(String ailacharyalat) {
        this.ailacharyalat = ailacharyalat;
    }

    /**
     * @return The ailacharyalng
     */
    public String getAilacharyalng() {
        return ailacharyalng;
    }

    /**
     * @param ailacharyalng The ailacharyalng
     */
    public void setAilacharyalng(String ailacharyalng) {
        this.ailacharyalng = ailacharyalng;
    }

    /**
     * @return The ailacharyaplace
     */
    public String getAilacharyaplace() {
        return ailacharyaplace;
    }

    /**
     * @param ailacharyaplace The ailacharyaplace
     */
    public void setAilacharyaplace(String ailacharyaplace) {
        this.ailacharyaplace = ailacharyaplace;
    }

    /**
     * @return The upadhyayid
     */
    public String getUpadhyayid() {
        return upadhyayid;
    }

    /**
     * @param upadhyayid The upadhyayid
     */
    public void setUpadhyayid(String upadhyayid) {
        this.upadhyayid = upadhyayid;
    }

    /**
     * @return The upadhyayname
     */
    public String getUpadhyayname() {
        return upadhyayname;
    }

    /**
     * @param upadhyayname The upadhyayname
     */
    public void setUpadhyayname(String upadhyayname) {
        this.upadhyayname = upadhyayname;
    }

    /**
     * @return The upadhyaydate
     */
    public String getUpadhyaydate() {
        return upadhyaydate;
    }

    /**
     * @param upadhyaydate The upadhyaydate
     */
    public void setUpadhyaydate(String upadhyaydate) {
        this.upadhyaydate = upadhyaydate;
    }

    /**
     * @return The upadhyayguru
     */
    public String getUpadhyayguru() {
        return upadhyayguru;
    }

    /**
     * @param upadhyayguru The upadhyayguru
     */
    public void setUpadhyayguru(String upadhyayguru) {
        this.upadhyayguru = upadhyayguru;
    }

    /**
     * @return The upadhyaylat
     */
    public String getUpadhyaylat() {
        return upadhyaylat;
    }

    /**
     * @param upadhyaylat The upadhyaylat
     */
    public void setUpadhyaylat(String upadhyaylat) {
        this.upadhyaylat = upadhyaylat;
    }

    /**
     * @return The upadhyaylng
     */
    public String getUpadhyaylng() {
        return upadhyaylng;
    }

    /**
     * @param upadhyaylng The upadhyaylng
     */
    public void setUpadhyaylng(String upadhyaylng) {
        this.upadhyaylng = upadhyaylng;
    }

    /**
     * @return The upadhyayplace
     */
    public String getUpadhyayplace() {
        return upadhyayplace;
    }

    /**
     * @param upadhyayplace The upadhyayplace
     */
    public void setUpadhyayplace(String upadhyayplace) {
        this.upadhyayplace = upadhyayplace;
    }

    /**
     * @return The muniid
     */
    public String getMuniid() {
        return muniid;
    }

    /**
     * @param muniid The muniid
     */
    public void setMuniid(String muniid) {
        this.muniid = muniid;
    }

    /**
     * @return The muniname
     */
    public String getMuniname() {
        return muniname;
    }

    /**
     * @param muniname The muniname
     */
    public void setMuniname(String muniname) {
        this.muniname = muniname;
    }

    /**
     * @return The munidate
     */
    public String getMunidate() {
        return munidate;
    }

    /**
     * @param munidate The munidate
     */
    public void setMunidate(String munidate) {
        this.munidate = munidate;
    }

    /**
     * @return The muniguru
     */
    public String getMuniguru() {
        return muniguru;
    }

    /**
     * @param muniguru The muniguru
     */
    public void setMuniguru(String muniguru) {
        this.muniguru = muniguru;
    }

    /**
     * @return The munilat
     */
    public String getMunilat() {
        return munilat;
    }

    /**
     * @param munilat The munilat
     */
    public void setMunilat(String munilat) {
        this.munilat = munilat;
    }

    /**
     * @return The munilng
     */
    public String getMunilng() {
        return munilng;
    }

    /**
     * @param munilng The munilng
     */
    public void setMunilng(String munilng) {
        this.munilng = munilng;
    }

    /**
     * @return The muniplace
     */
    public String getMuniplace() {
        return muniplace;
    }

    /**
     * @param muniplace The muniplace
     */
    public void setMuniplace(String muniplace) {
        this.muniplace = muniplace;
    }

    /**
     * @return The ailakid
     */
    public String getAilakid() {
        return ailakid;
    }

    /**
     * @param ailakid The ailakid
     */
    public void setAilakid(String ailakid) {
        this.ailakid = ailakid;
    }

    /**
     * @return The ailakname
     */
    public String getAilakname() {
        return ailakname;
    }

    /**
     * @param ailakname The ailakname
     */
    public void setAilakname(String ailakname) {
        this.ailakname = ailakname;
    }

    /**
     * @return The ailakdate
     */
    public String getAilakdate() {
        return ailakdate;
    }

    /**
     * @param ailakdate The ailakdate
     */
    public void setAilakdate(String ailakdate) {
        this.ailakdate = ailakdate;
    }

    /**
     * @return The ailakguru
     */
    public String getAilakguru() {
        return ailakguru;
    }

    /**
     * @param ailakguru The ailakguru
     */
    public void setAilakguru(String ailakguru) {
        this.ailakguru = ailakguru;
    }

    /**
     * @return The ailaklat
     */
    public String getAilaklat() {
        return ailaklat;
    }

    /**
     * @param ailaklat The ailaklat
     */
    public void setAilaklat(String ailaklat) {
        this.ailaklat = ailaklat;
    }

    /**
     * @return The ailaklng
     */
    public String getAilaklng() {
        return ailaklng;
    }

    /**
     * @param ailaklng The ailaklng
     */
    public void setAilaklng(String ailaklng) {
        this.ailaklng = ailaklng;
    }

    /**
     * @return The ailakplace
     */
    public String getAilakplace() {
        return ailakplace;
    }

    /**
     * @param ailakplace The ailakplace
     */
    public void setAilakplace(String ailakplace) {
        this.ailakplace = ailakplace;
    }

    /**
     * @return The kid
     */
    public String getKid() {
        return kid;
    }

    /**
     * @param kid The kid
     */
    public void setKid(String kid) {
        this.kid = kid;
    }

    /**
     * @return The kname
     */
    public String getKname() {
        return kname;
    }

    /**
     * @param kname The kname
     */
    public void setKname(String kname) {
        this.kname = kname;
    }

    /**
     * @return The kdate
     */
    public String getKdate() {
        return kdate;
    }

    /**
     * @param kdate The kdate
     */
    public void setKdate(String kdate) {
        this.kdate = kdate;
    }

    /**
     * @return The kguru
     */
    public String getKguru() {
        return kguru;
    }

    /**
     * @param kguru The kguru
     */
    public void setKguru(String kguru) {
        this.kguru = kguru;
    }

    /**
     * @return The klat
     */
    public String getKlat() {
        return klat;
    }

    /**
     * @param klat The klat
     */
    public void setKlat(String klat) {
        this.klat = klat;
    }

    /**
     * @return The klng
     */
    public String getKlng() {
        return klng;
    }

    /**
     * @param klng The klng
     */
    public void setKlng(String klng) {
        this.klng = klng;
    }

    /**
     * @return The kplace
     */
    public String getKplace() {
        return kplace;
    }

    /**
     * @param kplace The kplace
     */
    public void setKplace(String kplace) {
        this.kplace = kplace;
    }

    /**
     * @return The ganiniid
     */
    public String getGaniniid() {
        return ganiniid;
    }

    /**
     * @param ganiniid The ganiniid
     */
    public void setGaniniid(String ganiniid) {
        this.ganiniid = ganiniid;
    }

    /**
     * @return The ganinidate
     */
    public String getGaninidate() {
        return ganinidate;
    }

    /**
     * @param ganinidate The ganinidate
     */
    public void setGaninidate(String ganinidate) {
        this.ganinidate = ganinidate;
    }

    /**
     * @return The ganiniguru
     */
    public String getGaniniguru() {
        return ganiniguru;
    }

    /**
     * @param ganiniguru The ganiniguru
     */
    public void setGaniniguru(String ganiniguru) {
        this.ganiniguru = ganiniguru;
    }

    /**
     * @return The ganinilat
     */
    public String getGaninilat() {
        return ganinilat;
    }

    /**
     * @param ganinilat The ganinilat
     */
    public void setGaninilat(String ganinilat) {
        this.ganinilat = ganinilat;
    }

    /**
     * @return The ganinilng
     */
    public String getGaninilng() {
        return ganinilng;
    }

    /**
     * @param ganinilng The ganinilng
     */
    public void setGaninilng(String ganinilng) {
        this.ganinilng = ganinilng;
    }

    /**
     * @return The ganiniplace
     */
    public String getGaniniplace() {
        return ganiniplace;
    }

    /**
     * @param ganiniplace The ganiniplace
     */
    public void setGaniniplace(String ganiniplace) {
        this.ganiniplace = ganiniplace;
    }

    /**
     * @return The aryikaid
     */
    public String getAryikaid() {
        return aryikaid;
    }

    /**
     * @param aryikaid The aryikaid
     */
    public void setAryikaid(String aryikaid) {
        this.aryikaid = aryikaid;
    }

    /**
     * @return The aryikadate
     */
    public String getAryikadate() {
        return aryikadate;
    }

    /**
     * @param aryikadate The aryikadate
     */
    public void setAryikadate(String aryikadate) {
        this.aryikadate = aryikadate;
    }

    /**
     * @return The aryikaguru
     */
    public String getAryikaguru() {
        return aryikaguru;
    }

    /**
     * @param aryikaguru The aryikaguru
     */
    public void setAryikaguru(String aryikaguru) {
        this.aryikaguru = aryikaguru;
    }

    /**
     * @return The aryikalat
     */
    public String getAryikalat() {
        return aryikalat;
    }

    /**
     * @param aryikalat The aryikalat
     */
    public void setAryikalat(String aryikalat) {
        this.aryikalat = aryikalat;
    }

    /**
     * @return The aryikalng
     */
    public String getAryikalng() {
        return aryikalng;
    }

    /**
     * @param aryikalng The aryikalng
     */
    public void setAryikalng(String aryikalng) {
        this.aryikalng = aryikalng;
    }

    /**
     * @return The aryikaplace
     */
    public String getAryikaplace() {
        return aryikaplace;
    }

    /**
     * @param aryikaplace The aryikaplace
     */
    public void setAryikaplace(String aryikaplace) {
        this.aryikaplace = aryikaplace;
    }

    /**
     * @return The kshullikaid
     */
    public String getKshullikaid() {
        return kshullikaid;
    }

    /**
     * @param kshullikaid The kshullikaid
     */
    public void setKshullikaid(String kshullikaid) {
        this.kshullikaid = kshullikaid;
    }

    /**
     * @return The kshullikadate
     */
    public String getKshullikadate() {
        return kshullikadate;
    }

    /**
     * @param kshullikadate The kshullikadate
     */
    public void setKshullikadate(String kshullikadate) {
        this.kshullikadate = kshullikadate;
    }

    /**
     * @return The kshullikaguru
     */
    public String getKshullikaguru() {
        return kshullikaguru;
    }

    /**
     * @param kshullikaguru The kshullikaguru
     */
    public void setKshullikaguru(String kshullikaguru) {
        this.kshullikaguru = kshullikaguru;
    }

    /**
     * @return The kshullikalat
     */
    public String getKshullikalat() {
        return kshullikalat;
    }

    /**
     * @param kshullikalat The kshullikalat
     */
    public void setKshullikalat(String kshullikalat) {
        this.kshullikalat = kshullikalat;
    }

    /**
     * @return The kshullikalng
     */
    public String getKshullikalng() {
        return kshullikalng;
    }

    /**
     * @param kshullikalng The kshullikalng
     */
    public void setKshullikalng(String kshullikalng) {
        this.kshullikalng = kshullikalng;
    }

    /**
     * @return The kshullikaplace
     */
    public String getKshullikaplace() {
        return kshullikaplace;
    }

    /**
     * @param kshullikaplace The kshullikaplace
     */
    public void setKshullikaplace(String kshullikaplace) {
        this.kshullikaplace = kshullikaplace;
    }

    /**
     * @return The bhramcharyaid
     */
    public String getBhramcharyaid() {
        return bhramcharyaid;
    }

    /**
     * @param bhramcharyaid The bhramcharyaid
     */
    public void setBhramcharyaid(String bhramcharyaid) {
        this.bhramcharyaid = bhramcharyaid;
    }

    /**
     * @return The bhramcharyadate
     */
    public String getBhramcharyadate() {
        return bhramcharyadate;
    }

    /**
     * @param bhramcharyadate The bhramcharyadate
     */
    public void setBhramcharyadate(String bhramcharyadate) {
        this.bhramcharyadate = bhramcharyadate;
    }

    /**
     * @return The bhramcharyaguru
     */
    public String getBhramcharyaguru() {
        return bhramcharyaguru;
    }

    /**
     * @param bhramcharyaguru The bhramcharyaguru
     */
    public void setBhramcharyaguru(String bhramcharyaguru) {
        this.bhramcharyaguru = bhramcharyaguru;
    }

    /**
     * @return The bhramcharyalat
     */
    public String getBhramcharyalat() {
        return bhramcharyalat;
    }

    /**
     * @param bhramcharyalat The bhramcharyalat
     */
    public void setBhramcharyalat(String bhramcharyalat) {
        this.bhramcharyalat = bhramcharyalat;
    }

    /**
     * @return The bhramcharyalng
     */
    public String getBhramcharyalng() {
        return bhramcharyalng;
    }

    /**
     * @param bhramcharyalng The bhramcharyalng
     */
    public void setBhramcharyalng(String bhramcharyalng) {
        this.bhramcharyalng = bhramcharyalng;
    }

    /**
     * @return The bhramcharyaplace
     */
    public String getBhramcharyaplace() {
        return bhramcharyaplace;
    }

    /**
     * @param bhramcharyaplace The bhramcharyaplace
     */
    public void setBhramcharyaplace(String bhramcharyaplace) {
        this.bhramcharyaplace = bhramcharyaplace;
    }

    /**
     * @return The mid
     */
    public String getMid() {
        return mid;
    }

    /**
     * @param mid The mid
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    /**
     * @return The lat
     */
    public String getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     * @return The lng
     */
    public String getLng() {
        return lng;
    }

    /**
     * @param lng The lng
     */
    public void setLng(String lng) {
        this.lng = lng;
    }

    /**
     * @return The location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return The locality
     */
    public String getLocality() {
        return locality;
    }

    /**
     * @param locality The locality
     */
    public void setLocality(String locality) {
        this.locality = locality;
    }

    /**
     * @return The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return The contactid
     */
    public String getContactid() {
        return contactid;
    }

    /**
     * @param contactid The contactid
     */
    public void setContactid(String contactid) {
        this.contactid = contactid;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * @param website The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * @return The facebook
     */
    public String getFacebook() {
        return facebook;
    }

    /**
     * @param facebook The facebook
     */
    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    /**
     * @return The gplus
     */
    public String getGplus() {
        return gplus;
    }

    /**
     * @param gplus The gplus
     */
    public void setGplus(String gplus) {
        this.gplus = gplus;
    }

    /**
     * @return The youtube
     */
    public String getYoutube() {
        return youtube;
    }

    /**
     * @param youtube The youtube
     */
    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    /**
     * @return The wikipedia
     */
    public String getWikipedia() {
        return wikipedia;
    }

    /**
     * @param wikipedia The wikipedia
     */
    public void setWikipedia(String wikipedia) {
        this.wikipedia = wikipedia;
    }

}