package com.architjn.jainmunilocator.Models;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by audiocompass on 27/05/16.
 */
public class MuniMapModel implements ClusterItem {

    public final String name;
    public final int id;
    public final MuniDataType muniDataType;
    private final LatLng mPositon;


    public MuniMapModel(String name, LatLng mPositon, int id, MuniDataType muniDataType) {
        this.name = name;
        this.mPositon = mPositon;
        this.id = id;
        this.muniDataType = muniDataType;
    }

    public String getName() {
        return name;
    }


    public int getId() {
        return id;
    }

    @Override
    public LatLng getPosition() {
        return mPositon;
    }

    public MuniDataType getMuniDataType() {
        return muniDataType;
    }
}

