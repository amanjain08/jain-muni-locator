package com.architjn.jainmunilocator.Models;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by audiocompass on 29/05/16.
 */
public class HeaderDetailModel {
    private static final String TAG = "HeaderDetail";
    private String name;
    protected String title;
    protected String lastLocation;
    protected String time;

    public HeaderDetailModel(String title, String name, String lastLocation, String time) {
        this.name = name;
        this.title = title;
        this.lastLocation = lastLocation;
        this.time = time;
    }

    public HeaderDetailModel() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastLocation() {
        if(lastLocation.isEmpty())
            return  "Not Available";
        return lastLocation;
    }

    public void setLastLocation(String lastLocation) {
        this.lastLocation = lastLocation;
    }

    public String getTime() {
        if (title != null && title.equalsIgnoreCase("Samaadhi Detail")) {
            if (!time.equalsIgnoreCase("0000-00-00")) {
                String finalDate = "0000-00-00";
                SimpleDateFormat myTime = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat wantsTime = new SimpleDateFormat("EEEE, dd MMMM yyyy");
                try {
                    finalDate = wantsTime.format(myTime.parse(time));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return finalDate;
            } else return "Not Available";

        } else if (!time.equalsIgnoreCase("0000-00-00 00:00:00")) {
            String finalDate = "0000-00-00 00:00:00";
            SimpleDateFormat myTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat wantsTime = new SimpleDateFormat("EEEE, dd MMM yyyy, hh:mm a");
            try {
                finalDate = wantsTime.format(myTime.parse(time));
                Log.d(TAG, "getTime: "+finalDate.toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return finalDate;
        } else return "Not Available";

    }

    public void setTime(String time) {
        this.time = time;
    }
}
