package com.architjn.jainmunilocator.Models;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by audiocompass on 29/05/16.
 */
public class HistoryDataModel {
    protected String birthName;
    protected String dob;
    protected String birthPlace;
    protected String fatherName;
    protected String motherName;
    private String kid;
    protected String education;
    protected String birhtLat;
    protected String birthLng;


    public HistoryDataModel(String birthName, String dob, String birthPlace, String fatherName, String motherName, String kid,String education,String birhtLat,String birthLng) {

        this.birthName = birthName;
        this.dob = dob;
        this.birthPlace = birthPlace;
        this.fatherName = fatherName;
        this.motherName = motherName;
        this.kid = kid;
        this.education = education;
        this.birhtLat = birhtLat;
        this.birthLng = birthLng;

    }

    public HistoryDataModel() {

    }

    public String getBirthName() {
        return birthName;
    }

    public void setBirthName(String birthName) {
        this.birthName = birthName;
    }

    public String getDob() {
        if (!dob.equalsIgnoreCase("0000-00-00")) {
            String finalDate = "0000-00-00";
            SimpleDateFormat myTime = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat wantsTime = new SimpleDateFormat("EEEE, dd MMMm yyyy");
            try {
                finalDate = wantsTime.format(myTime.parse(dob));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return finalDate;
        } else return dob;
    }

    public String getBirhtLat() {
        return birhtLat;
    }

    public String getBirthLng() {
        return birthLng;
    }

    public void setBirhtLat(String birhtLat) {
        this.birhtLat = birhtLat;
    }



    public void setBirthLng(String birthLng) {
        this.birthLng = birthLng;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getKid() {
        return kid;
    }

    public void setKid(String kid) {
        this.kid = kid;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }
}
