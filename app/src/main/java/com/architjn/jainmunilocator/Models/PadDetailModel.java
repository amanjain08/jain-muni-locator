package com.architjn.jainmunilocator.Models;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by audiocompass on 29/05/16.
 */
public class PadDetailModel {
    public String prefix;
    public String title;
    public String id;
    public String name;
    public String date;
    public String guruId;
    public String place;
    public String lat;
    public String lon;

    public PadDetailModel(String prefix,String title,String id, String name, String date, String guruId, String place, String lat, String lon) {
        this.title = title;
        this.id = id;
        this.name = name;
        this.date = date;
        this.guruId = guruId;
        this.place = place;
        this.lat = lat;
        this.lon = lon;
        this.prefix= prefix;
    }

    public PadDetailModel() {

    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        if (!date.equalsIgnoreCase("0000-00-00")) {
            String finalDate = "0000-00-00";
            SimpleDateFormat myTime = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat wantsTime = new SimpleDateFormat("EEEE, dd MMMM yyyy");
            try {
                finalDate = wantsTime.format(myTime.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return finalDate;
        } else return date;

    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getGuruId() {
        return Integer.parseInt(guruId);
    }

    public void setGuruId(String guruId) {
        this.guruId = guruId;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Double getLat() {
        return Double.parseDouble(lat);
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return Double.parseDouble(lon);
    }

    public void setLon(String lon) {
        this.lon = lon;
    }
}
