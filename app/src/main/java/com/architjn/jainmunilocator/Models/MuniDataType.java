package com.architjn.jainmunilocator.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by architjn on 21/02/16.
 */
public class MuniDataType implements Serializable {

    @SerializedName("mid")
    private String mid;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lon;
    @SerializedName("location")
    private String location;
    @SerializedName("locality")
    private String locality;
    @SerializedName("id")
    private String id;
    @SerializedName("upadhi")
    private String upadhi;
    @SerializedName("title")
    private String title;
    @SerializedName("name")
    private String name;
    @SerializedName("alias")
    private String alias;
    @SerializedName("vairagya")
    private String vairagya;
    @SerializedName("birthname")
    private String birthName;
    @SerializedName("dob")
    private String dob;
    @SerializedName("father")
    private String fatherName;
    @SerializedName("mother")
    private String motherName;
    @SerializedName("img")
    private String imgPath;
    @SerializedName("approved")
    private String approved;
    @SerializedName("dos")
    private String dos;
    @SerializedName("grahtyag")
    private String grahTyag;
    @SerializedName("education")
    private String education;
    @SerializedName("uid")
    private String uid;
    @SerializedName("uname")
    private String uname;
    @SerializedName("prefix")
    private String prefix;
    @SerializedName("suffix")
    private String suffix;

    public MuniDataType(String mid, String lat, String lon, String location, String locality, String id, String upadhi, String title, String name, String alias, String vairagya, String birthName, String dob, String fatherName, String motherName, String imgPath, String approved, String dos, String grahTyag, String education, String uid, String uname, String prefix, String suffix) {
        this.mid = mid;
        this.lat = lat;
        this.lon = lon;
        this.location = location;
        this.locality = locality;
        this.id = id;
        this.upadhi = upadhi;
        this.title = title;
        this.name = name;
        this.alias = alias;
        this.vairagya = vairagya;
        this.birthName = birthName;
        this.dob = dob;
        this.fatherName = fatherName;
        this.motherName = motherName;
        this.imgPath = imgPath;
        this.approved = approved;
        this.dos = dos;
        this.grahTyag = grahTyag;
        this.education = education;
        this.uid = uid;
        this.uname = uname;
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Double getLat() {
        return Double.parseDouble(lat);
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return Double.parseDouble(lon);
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpadhi() {
        return upadhi;
    }

    public void setUpadhi(String upadhi) {
        this.upadhi = upadhi;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getVairagya() {
        return vairagya;
    }

    public void setVairagya(String vairagya) {
        this.vairagya = vairagya;
    }

    public String getBirthName() {
        return birthName;
    }

    public void setBirthName(String birthName) {
        this.birthName = birthName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public String getDos() {
        return dos;
    }

    public void setDos(String dos) {
        this.dos = dos;
    }

    public String getGrahTyag() {
        return grahTyag;
    }

    public void setGrahTyag(String grahTyag) {
        this.grahTyag = grahTyag;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

}
