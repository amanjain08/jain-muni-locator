package com.architjn.jainmunilocator.AppUtil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.architjn.jainmunilocator.AppController;
import com.architjn.jainmunilocator.MonitorService;

public class LaunchReceiver extends BroadcastReceiver {

    public static final String ACTION_PULSE_SERVER_ALARM = "com.architjn.jainmunilocator.ACTION_PULSE_SERVER_ALARM";
    private static final String TAG = "LaunchReciver";

    @Override
    public void onReceive(Context context, Intent intent) {
        // AppGlobal.logDebug("OnReceive for " + intent.getAction());
        //  AppGlobal.logDebug(intent.getExtras().toString());
        Log.d(TAG, "onReceive: ");
        Intent serviceIntent = new Intent(AppController.getContext(), MonitorService.class);
        AppController.getContext().startService(serviceIntent);
    }
}