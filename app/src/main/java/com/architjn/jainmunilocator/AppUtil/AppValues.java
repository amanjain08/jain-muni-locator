package com.architjn.jainmunilocator.AppUtil;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.util.Log;
import android.widget.ImageView;

import com.architjn.jainmunilocator.Models.CompleteMuniDetail;
import com.architjn.jainmunilocator.Models.MuniDataType;
import com.architjn.jainmunilocator.R;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by Aman Jain on 13/04/16.
 */
public class AppValues {
    public static boolean isLoggedIn;
    public static String name;
    public static String email;
    public static String imgUrl;
    public static String userID;
    public static String loginAgent;
    public static boolean isGettingImage = false;
    private static AppValues ourInstance = new AppValues();
    public static String MIXPANEL_TOKEN = "ba58d07921ceca7730c0c7ebcdce3a88";
    public static int NORMALRUN = 0;
    public static int FIRSTRUN = 1;
    public static int UPGRADE = 2;
    public static boolean isFirstLaunch = false;

    public static final String MIXPANELDATA = "MIXPANELDATA";
    public static final String KEY_FIRST_TIME_INSTALLATION = "first_time_installation";
    public static final String KEY_UNIQUE_USER_ID = "unique_user_id";

    public static AppValues getInstance() {
        return ourInstance;
    }

    public static String getDeviceID(Context mActivity) {
        return Settings.Secure.getString(mActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    public void getData(Context mActivity) {
        SharedPreferences sharedPreferences = mActivity.getSharedPreferences("LOGIN_DATA", Context.MODE_PRIVATE);
        name = sharedPreferences.getString("name", null);
        email = sharedPreferences.getString("email", null);
        imgUrl = sharedPreferences.getString("img", null);
        isLoggedIn = sharedPreferences.getBoolean("login", false);
        userID = sharedPreferences.getString("id", null);
        loginAgent = sharedPreferences.getString("agent", null);
        email = getEmail(mActivity);
    }

    public static boolean isInternetConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();

    }

    public static String checkString(String str) {
        if (str != null && !str.isEmpty() && !str.equalsIgnoreCase("N/A") && !str.equalsIgnoreCase("0000-00-00")) {
            return str;
        }
        return "Not Available";
    }

    public static String isData(String str) {
        if (!checkString(str).equalsIgnoreCase("Not Available")) {
            return str;
        }
        return "";
    }

    public static String getName(MuniDataType data) {
        return data.getUname() + " " + data.getPrefix() + " " + data.getName() + " " + data.getSuffix() + " " + data.getAlias();
    }

    public static String getName(CompleteMuniDetail data) {
        return data.getUname() + " " + data.getPrefix() + " " + data.getName() + " " + data.getSuffix() + " " + data.getAlias();
    }


    public static boolean checkData(String name, String date, String guru, String place) {
        if (name != null && !name.isEmpty() || date != null && !date.equalsIgnoreCase("0000-00-00") || guru != null && !guru.equalsIgnoreCase("0") || place != null && !place.equalsIgnoreCase("N/A"))
            return true;
        return false;
    }

    public static class MarkerCallback implements Callback {
        private Marker marker;
        private ImageView imageView;
        private MuniDataType muni;
        private int retry;
        private Activity mActivity;

        public MarkerCallback(Activity mActivity, Marker marker, ImageView imageView, MuniDataType muni, int retry) {
            this.imageView = imageView;
            this.muni = muni;
            this.marker = marker;
            this.retry = retry;
            this.mActivity = mActivity;
        }

        @Override
        public void onError() {
            Log.e(getClass().getSimpleName(), "Error loading thumbnail!");
            if (!muni.getImgPath().equalsIgnoreCase("na.png")) {
                String imgUrl = muni.getImgPath();
                Log.d("ImageCallBack", "onError: REtry count : " + retry);
                Log.d("ImageCallBack", "onError: " + imgUrl);
                if (retry > 0) {
                    Picasso.with(mActivity).load(imgUrl).error(R.drawable.not_available).into(imageView, new MarkerCallback(mActivity, marker, imageView, muni, --retry));
                } else {
                    imageView.setImageResource(R.drawable.not_available);
                }
            } else imageView.setImageResource(R.drawable.not_available);
        }

        @Override
        public void onSuccess() {
            if (marker != null && marker.isInfoWindowShown()) {
                isGettingImage = true;
                marker.hideInfoWindow();
                marker.hideInfoWindow();
                marker.showInfoWindow();
            }
        }
    }

    public static Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = it.next();
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
                Log.d("", "checkParams: " + pairs.getKey() + " : " + pairs.getValue());
            }
        }
        return map;
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    public static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);
        if (email != null && !email.isEmpty()) {
            return email;
        } else if (account != null) {
            return account.name;
        } else
            return "";
    }

    public static void setMixPanelUniqueUserId(Context context, String id) {
        context.getSharedPreferences(MIXPANELDATA, Context.MODE_PRIVATE)
                .edit()
                .putString(KEY_UNIQUE_USER_ID, id)
                .commit();
    }

    public static String getMixPanelUniqueUserId(Context context) {
        return context.getSharedPreferences(MIXPANELDATA, Context.MODE_PRIVATE)
                .getString(KEY_UNIQUE_USER_ID, "");
    }

    public static int checkFirstRun(Activity mActivity) {

        final String PREFS_NAME = "MyPrefsFile";
        final String PREF_VERSION_CODE_KEY = "version_code";
        final int DOESNT_EXIST = -1;


        // Get current version code
        int currentVersionCode = 0;
        try {
            currentVersionCode = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            // handle exception
            e.printStackTrace();
            return -1;
        }

        // Get saved version code
        SharedPreferences prefs = mActivity.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        // Update the shared preferences with the current version code
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).commit();

        // Check for first run or upgrade
        if (currentVersionCode == savedVersionCode) {

            isFirstLaunch = false;
            // This is just a normal run
            return NORMALRUN;

        } else if (savedVersionCode == DOESNT_EXIST) {
            isFirstLaunch = true;
            return FIRSTRUN;
            // TODO This is a new install (or the user cleared the shared preferences)

        } else if (currentVersionCode > savedVersionCode) {
            return UPGRADE;
            // TODO This is an upgrade

        } else
            return -1;
    }


}
