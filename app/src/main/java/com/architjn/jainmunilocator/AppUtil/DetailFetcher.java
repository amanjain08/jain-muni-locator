package com.architjn.jainmunilocator.AppUtil;

import android.os.AsyncTask;
import android.util.Log;

import com.architjn.jainmunilocator.Activity.MainActivity;
import com.architjn.jainmunilocator.Models.MuniDataType;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by architjn on 21/02/16.
 */
public class DetailFetcher extends AsyncTask<Void, Void, Void> {
    private String jsonResult;
    private String url;

    public DetailFetcher(String url) {
        this.url = url;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        HttpClient httpclient = new DefaultHttpClient();
        httpclient.getConnectionManager().closeExpiredConnections();
        HttpPost httppost = new HttpPost(url);
        try {
            httppost.setEntity(new UrlEncodedFormEntity(params));
            Log.v("TAG", url);
            HttpResponse response = httpclient.execute(httppost);
            jsonResult = inputStreamToString(response.getEntity().getContent()).toString();

        } catch (ClientProtocolException e) {
            if (MainActivity.progressDialog != null) {
                MainActivity.progressDialog.cancel();
            }
            //final Snackbar snackbar = Snackbar.make(MainActivity.coordinatorLayout, "Error Occured", Snackbar.LENGTH_LONG);
            //snackbar.show();
            Log.e("e", "error1");
            e.printStackTrace();
            onDetailError();
        } catch (IOException e) {
            if (MainActivity.progressDialog != null) {
                MainActivity.progressDialog.cancel();
            }
            // final Snackbar snackbar = Snackbar.make(MainActivity.coordinatorLayout, "Error Occured", Snackbar.LENGTH_LONG);
            //snackbar.show();
            Log.e("e", "error2");
            e.printStackTrace();
            onDetailError();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        fetchDetails();
    }

    private void fetchDetails() {
        if (jsonResult != null) {
            Log.d("DetailFethcer", jsonResult);
            ArrayList<MuniDataType> munis = new ArrayList<MuniDataType>();
            List<MuniDataType> list = new Gson().fromJson(jsonResult, new TypeToken<List<MuniDataType>>() {
            }.getType());
            ArrayList<String> muniName = new ArrayList<>();
            HashMap<String, Integer> muniNameID = new HashMap<>();
            munis.clear();
            int i = 0;
            for (MuniDataType data : list) {
                munis.add(data);

                //Log.d("Fetcher",data.toString());
            }
            Collections.sort(munis, new Comparator<MuniDataType>() {
                @Override
                public int compare(MuniDataType lhs, MuniDataType rhs) {
                    return Integer.parseInt(lhs.getId()) - Integer.parseInt(rhs.getId());
                }
            });
            for (MuniDataType data : munis) {
                muniName.add(AppValues.getName(data));
                Log.d("Aman", "fetchDetails: Muni ID:  " + data.getId() + "   " + i);
                muniNameID.put(AppValues.getName(data), i++);
            }
            onDetailsFetched(munis, muniName, muniNameID);
        }


    }

    private StringBuilder inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public void onDetailsFetched(ArrayList<MuniDataType> muniList, ArrayList<String> muniName, HashMap<String, Integer> muniNameID) {
    }

    public void onDetailError() {

    }

}
