/*
package com.architjn.jainmunilocator.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.architjn.jainmunilocator.Activity.MainActivity;
import com.architjn.jainmunilocator.AppUtil.AppValues;
import com.architjn.jainmunilocator.Models.MuniDataType;
import com.architjn.jainmunilocator.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

*
 * Created by architjn on 13/02/16.


public class MapFragment extends SupportMapFragment implements GoogleApiClient.ConnectionCallbacks, MainActivity.SearchPlace,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener {

    private static final String TAG = "MapFragment";
    private final int[] MAP_TYPES = {GoogleMap.MAP_TYPE_SATELLITE,
            GoogleMap.MAP_TYPE_NORMAL,
            GoogleMap.MAP_TYPE_HYBRID,
            GoogleMap.MAP_TYPE_TERRAIN,
            GoogleMap.MAP_TYPE_NONE};
    Marker prevMarker;
    private GoogleApiClient mGoogleApiClient;
    private Location mCurrentLocation;
    private int curMapTypeIndex = 1;
    private ArrayList<MuniDataType> muniList;
    public ImageView img;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).setSearchPlace(this);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        initListeners();
    }

    private void initListeners() {
        getMap().setOnMarkerClickListener(this);
        getMap().setOnMapLongClickListener(this);
        getMap().setOnInfoWindowClickListener(this);
        getMap().setOnMapClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        initCamera(new LatLng(21.0000, 78.0000));
    }

    @Override
    public void onMapClick(LatLng latLng) {

//        MarkerOptions options = new MarkerOptions().position(latLng);
//        options.title(getAddressFromLatLng(latLng));
//
//        options.icon(BitmapDescriptorFactory.defaultMarker());
//        getMap().addMarker(options);
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
    }

    private String getAddressFromLatLng(LatLng latLng) {
        Geocoder geocoder = new Geocoder(getActivity());

        String address = "";
        try {
            address = geocoder
                    .getFromLocation(latLng.latitude, latLng.longitude, 1)
                    .get(0).getAddressLine(0);
        } catch (IOException e) {
        }

        return address;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (marker.equals(prevMarker)) {
            marker.hideInfoWindow();
            prevMarker = null;
            return true;
        }
        double latitude = marker.getPosition().latitude + 1.0 / Math.pow(getMap().getCameraPosition().zoom, 3);
        LatLng latLng = new LatLng(latitude, marker.getPosition().longitude);
        getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, getMap().getCameraPosition().zoom + 2f), 1000, null);
        prevMarker = marker;
        marker.showInfoWindow();
        return true;
    }

    private void initCamera(LatLng location) {
        CameraPosition position = CameraPosition.builder()
                .target(location)
                .zoom(4f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();

        getMap().animateCamera(CameraUpdateFactory.newCameraPosition(position), null);
        getMap().setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {
                MuniDataType muni = muniList.get(Integer.parseInt(arg0.getSnippet()));
                View v = MapFragment.this.getActivity().getLayoutInflater().inflate(R.layout.windowlayout, null);
                img = (ImageView) v.findViewById(R.id.img);
                TextView name = (TextView) v.findViewById(R.id.name);
                TextView loc = (TextView) v.findViewById(R.id.area);
                String imgUrl = "http://jainmunilocator.org/munis/uploads/" + muni.getUname().toLowerCase() + "_" + muni.getName().replaceAll(" ", "").toLowerCase() + ".jpg";
                Log.v("img", imgUrl);
                {
                    Picasso.with(getActivity()).load(imgUrl).into(img, new MarkerCallback(arg0, img, muni, 1));
                }
                name.setText(AppValues.getName(muni));
                if (muni.getLocation().isEmpty() || muni.getLocation().equalsIgnoreCase("N/A"))
                    loc.setText("Not Available");
                else {
                    loc.setText(muni.getLocation());
                }
                return v;

            }
        });
        getMap().setMapType(MAP_TYPES[curMapTypeIndex]);
        getMap().setTrafficEnabled(true);
        getMap().getUiSettings().setZoomControlsEnabled(true);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getMap().setMyLocationEnabled(true);


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        MuniDataType muni = muniList.get(Integer.parseInt(marker.getSnippet()));
Intent intent = new Intent(getActivity(), MuniDetail.class);
        intent.putExtra("munidata", muni);
        startActivity(intent);

        final String URL = "http://jainmunilocator.org/munis.php?id=" + muni.getId();
        ((MainActivity) getActivity()).webView.clearCache(true);
        ((MainActivity) getActivity()).webView.loadUrl("about:blank");

        ((MainActivity) getActivity()).webView.clearView();
        ((MainActivity) getActivity()).webView.getSettings().setUseWideViewPort(true);
        ((MainActivity) getActivity()).webView.getSettings().setLoadWithOverviewMode(true);
        ((MainActivity) getActivity()).webView.getSettings().setJavaScriptEnabled(true);
        ((MainActivity) getActivity()).webView.loadUrl(URL);
        showProgress();

        ((MainActivity) getActivity()).webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                // hide element by class name
                ((MainActivity) getActivity()).webView.loadUrl("javascript:(function() { " + "document.getElementsByClassName('navbar-custom')[0].style.display='none'; })()");
                if (!url.contains("about:blank"))
                    hideProgress();
                // hide element by id

            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                ((MainActivity) getActivity()).webView.loadUrl("javascript:(function() { " + "document.getElementsByClassName('navbar-custom')[0].style.display='none'; })()");
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                ((MainActivity) getActivity()).webView.loadUrl("javascript:(function() { " + "document.getElementsByClassName('navbar-custom')[0].style.display='none'; })()");
            }


        });
        MainActivity.slidingPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);


    }

    public void updateMuniList(ArrayList<MuniDataType> muniList) {
        this.muniList = muniList;
        for (int i = 0; i < muniList.size(); i++) {
            MarkerOptions options = new MarkerOptions().position(new LatLng(Double.parseDouble(muniList.get(i).getLat()), Double.parseDouble(muniList.get(i).getLon())));
            options.snippet(String.valueOf(i));
            options.icon(BitmapDescriptorFactory.defaultMarker());
            getMap().addMarker(options);
        }
        if (MainActivity.progressDialog != null) {
            MainActivity.progressDialog.cancel();
        }
    }

    public void closeButton() {
        prevMarker.hideInfoWindow();
    }

    @Override
    public void search(Place place) {
        getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 13f), 1000, null);
    }

    @Override
    public void searchMuni(int position) {
        View view = getView();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        LatLng latLng = new LatLng(Double.parseDouble(muniList.get(position).getLat()), Double.parseDouble(muniList.get(position).getLon()));
        MarkerOptions options = new MarkerOptions().position(latLng);
        options.snippet(String.valueOf(position));
        options.icon(BitmapDescriptorFactory.defaultMarker());
        Marker marker = getMap().addMarker(options);
        getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18f), 1200, null);
        marker.showInfoWindow();

    }

    public class MarkerCallback implements Callback {
        Marker marker;
        ImageView imageView;
        MuniDataType muni;
        int retry;

        public MarkerCallback(Marker marker, ImageView imageView, MuniDataType muni, int retry) {
            this.imageView = imageView;
            this.muni = muni;
            this.marker = marker;
            this.retry = retry;
        }

        @Override
        public void onError() {

            Log.e(getClass().getSimpleName(), "Error loading thumbnail!");
            if (!muni.getImgPath().equalsIgnoreCase("na.png")) {
                String imgUrl = muni.getImgPath();
                Log.d(TAG, "onError: REtry count : " + retry);
                Log.d(TAG, "onError: " + imgUrl);
                if (retry > 0) {
                    Picasso.with(getActivity()).load(imgUrl).error(R.drawable.not_available).into(img, new MarkerCallback(marker, imageView, muni, retry--));
                }
            } else imageView.setImageResource(R.drawable.not_available);
        }

        @Override
        public void onSuccess() {
            if (marker != null && marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
                marker.showInfoWindow();
            }
        }
    }

    public void showProgress() {
        if (MainActivity.circleProgressBar != null) {
            MainActivity.circleProgressBar.setVisibility(View.VISIBLE);
        }

    }

    public void hideProgress() {
        if (MainActivity.circleProgressBar != null) {
            MainActivity.circleProgressBar.setVisibility(View.INVISIBLE);
        }
    }
}
*/
